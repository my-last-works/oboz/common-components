import axios from 'axios'
import VueCookie from 'vue-cookie'

function apiStatus (response) {
  if (!response || typeof response === 'string') {
    throw new Error(response)
  }

  const { data, status, statusText } = response

  const serviceError = data && data.error
  const serviceErrorText = (serviceError && data.message) || serviceError
  const statusError = !status || ((status < 200 || status > 300))
  const errorValue = serviceErrorText || statusText || true

  if (status === 401 && process.env.NODE_ENV === 'production') {
  } else if (statusError || serviceError) {
    return { error: errorValue }
  } else {
    return { data }
  }
}

export default class RestClient {
  constructor (props = {}) {
    if ((!window.OBOZ_CONFIG || !window.OBOZ_CONFIG.OBOZ_API_HOST) && !process.env.VUE_APP_API_HOST) {
      console.error('api host not found')
    }
    this.instance = axios.create({
      baseURL: props.baseURL || (window.OBOZ_CONFIG && window.OBOZ_CONFIG.OBOZ_API_HOST) || process.env.VUE_APP_API_HOST
    })

    this.instance.interceptors.request.use((config = {}) => {
      let { params } = config

      if (params) {
        for (let key in params) {
          if (!params[key]) {
            delete params[key]
          }
        }
      }
      const token = VueCookie.get('auth_token')
      if (!token && process.env.NODE_ENV === 'production') {
      } else {
        config.headers = {
          Authorization: `Bearer ${token}`,
          'Accept-Version': 1,
          Accept: 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        }
      }
      return config
    }, (error) => {
      return Promise.reject(error)
    })

    this.instance.interceptors.response.use((response) => {
      return apiStatus(response)
    }, function ({ response } = {}) {
      return apiStatus(response)
    })
  }

  get (url, options = {}) {
    return this.instance.get(url, { ...options })
  }

  post (url, data = {}, options = {}) {
    return this.instance.post(url, data, { ...options })
  }

  put (url, data = {}, options = {}) {
    return this.instance.put(url, data, { ...options })
  }

  delete (url, options = {}) {
    return this.instance.delete(url, { ...options })
  }
}
