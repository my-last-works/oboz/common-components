import RestClient from './RestClient'

export const formationParameters = (page, size, q, sort, filters) => {
  let parameters = ''
  if (page !== undefined) parameters += '&page=' + page
  if (size !== undefined) parameters += '&size=' + size
  if (sort && sort.length > 0) {
    sort.forEach(i => {
      let _sort = i.desc ? 'desc' : 'asc'
      parameters += '&sort=' + i.selector + ',' + _sort
    })
  }
  if (filters) {
    for (let prop in filters) {
      if (filters[prop] !== undefined && filters[prop] !== null && filters[prop].length > 0) {
        parameters += '&' + prop + '=' + filters[prop]
      }
    }
  }
  if (q !== undefined && q.length > 0) parameters += '&q=' + q
  if (parameters.length > 0) {
    parameters = '?' + parameters
  }
  console.log(parameters)
  return parameters
}
const Client = new RestClient({ baseURL: 'http://localhost:3000' })

export default {
  getPageConstructor () {
    return Client.get('/')
  },
  getTableItems (url) {
    return Client.get('/' + url)
  },
  async getOptions (dictionaryName, page, size, q, sort) {
    const { data } = await Client.get(`/oboz2-dictionary-dictionaries-viewer/v1/dropdown/${dictionaryName}` + formationParameters(page, size, q, sort))
    const modified = {
      ...data,
      values: data.values.map(el => {
        return {
          ...el,
          title: `${(el.article_number ? el.article_number + ' – ' : '')}${(el.title || el.name)}`
        }
      })
    }

    return modified
  }
}
