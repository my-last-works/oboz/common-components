/* eslint-disable */
import RestClient from './RestClient'
let Client = new RestClient()

export default {
  async getLocations (withDeleted, page, size, q, sort, filters) {
    try {
      if (window.isMock) {
        return Promise.resolve({data: data.locations_list})
      } else {
        let res = await Client.get('/oboz2-dictionary-locations-crud/v1/' + formationParameters(withDeleted, page, size, q, sort, filters))
        return Promise.resolve(res)
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  async getLocation (uuid) {
    try {
      if (window.isMock) {
        return Promise.resolve({ data: data.location_detail })
      } else {
        let res = await Client.get('/oboz2-dictionary-locations-crud/v1/' + uuid)
        return Promise.resolve(res)
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  createLocation (data) {
    return Client.post('/oboz2-dictionary-locations-crud/v1/', data)
  },
  editLocation (uuid, data) {
    return Client.put('/oboz2-dictionary-locations-crud/v1/' + uuid, data)
  },
  deleteLocation (uuid) {
    return Client.put('/oboz2-dictionary-locations-crud/v1/' + uuid + '/true')
  },
  async getLocationTypes (withDeleted, page, size, q, sort) {
    try {
      if (window.isMock) {
        return Promise.resolve(data.location_types)
      } else {
        let res = await Client.get('/oboz2-dictionary-location-types-viewer/v1/' + formationParameters(withDeleted, page, size, q, sort))
        return Promise.resolve(res)
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  async getLocationTypesCount () {
    try {
      if (window.isMock) {
        return Promise.resolve(data.locations_count)
      } else {
        let res = await Client.get('/oboz2-dictionary-locations-crud/v1/counts')
        return Promise.resolve(res && res.data)
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  async getLocationType (uuid) {
    try {
      if (window.isMock) {
        return Promise.resolve(data.location_type_detail)
      } else {
        let res = await Client.get('/oboz2-dictionary-location-types-viewer/v1/' + uuid)
        return Promise.resolve(res)
      }
    } catch (e) {
      throw new Error(e)
    }
  }
}

const formationParameters = (withDeleted, page, size, q, sort, filters) => {
  let parameters = ''
  if (withDeleted !== undefined) parameters += 'withDeleted=' + withDeleted
  if (page !== undefined) parameters += '&page=' + page
  if (size !== undefined) parameters += '&size=' + size
  if (sort && sort.length > 0) {
    sort.forEach(i => {
      let _sort = i.desc ? 'desc' : 'asc'
      parameters += '&sort=' + i.selector + ',' + _sort
    })
  }
  if (filters) {
    for (let prop in filters) {
      if (filters[prop] !== undefined && filters[prop] !== null && filters[prop].length > 0) {
        parameters += '&' + prop + '=' + filters[prop]
      }
    }
  }
  if (q !== undefined && q.length > 0) parameters += '&q=' + q
  if (parameters.length > 0) {
    parameters = '?' + parameters
  }
  return parameters
}

const data = {
  "locations_count": [{
    'location_uuid': "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
    "location_name": "Аэропорт",
    "count": 37693
  }, {
    "location_uuid": "4adf706e-0be2-4383-b680-a73c959b9e2c",
    "location_name": "ЖД станция",
    "count": 18761
  }, {
    "location_uuid": "4adf706e-0be2-4383-b680-a73c959b9e4c",
    "location_name": "Контейнерный порт",
    "count": 437
  }, {
    "location_uuid": "4adf706e-0be2-4383-b680-a73c959b9e2f",
    "location_name": "Морской Порт",
    "count": 2706
  }, {
    "location_uuid": "5919a99f-d840-4855-b158-5ba957076603",
    "location_name": "Опорная точка",
    "count": 93
  }, {
    "location_uuid": "a2166b4d-27a9-41ce-9fc8-8bfd21baa8d7",
    "location_name": "Пограничный переход",
    "count": 57
  }, {
    "location_uuid": "4adf706e-0be2-4383-b680-a73c959b9e3c",
    "location_name": "Пункт таможенного оформления/СВХ",
    "count": 424
  }, {
    "location_uuid": "d375463b-d824-4a5b-bb04-099721c61c0f",
    "location_name": null,
    "count": 1
  }],
  "locations_list": {
    "content": [{
      "uuid": "5f98fd9e-ab21-4cd0-b905-62894d81ea83",
      "coordinates": {"lat": 53.20039749, "lon": 50.24769211},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Вятская ул., 13, Самара, Самарская обл., Россия, 443022",
      "english_address": "Vyatskaya Ulitsa, 13, Samara, Samarskaya oblast', Russia, 443022",
      "local_address": "Вятская ул., 13, Самара, Самарская обл., Россия, 443022",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 39},
      "is_active": true
    }, {
      "uuid": "6f74b7c6-d32e-4cfd-91f6-556093048533",
      "coordinates": {"lat": 56.32845272303, "lon": 38.1828550506781},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "тестовая точка 5",
      "english_name": "",
      "local_name": "",
      "russian_address": "Боголюбская ул., 1, Сергиев Посад, Московская обл., Россия, 141303",
      "english_address": "Bogolyubskaya Ulitsa, 1, Sergiev Posad, Moskovskaya oblast', Russia, 141303",
      "local_address": "Боголюбская ул., 1, Сергиев Посад, Московская обл., Россия, 141303",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "f43e3bd6-ec25-4a3b-860d-501f15da1cd7",
      "coordinates": {"lat": 56.328452782303, "lon": 38.18285510506781},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "тестовая точка 4",
      "english_name": "",
      "local_name": "",
      "russian_address": "Боголюбская ул., 1, Сергиев Посад, Московская обл., Россия, 141303",
      "english_address": "Bogolyubskaya Ulitsa, 1, Sergiev Posad, Moskovskaya oblast', Russia, 141303",
      "local_address": "Боголюбская ул., 1, Сергиев Посад, Московская обл., Россия, 141303",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "149aaf0d-2e5e-48af-98f4-36855655e68a",
      "coordinates": {"lat": 47.2072741, "lon": 39.6476128},
      "location_type_uuid": "4adf706e-0be2-4383-b680-a73c959b9e3c",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "ул. Тружеников, д.13, Ростов-на-Дону, Ростовская обл., Россия, 344058",
      "english_address": "Ulitsa Truzhenikov, д.13, Rostov-on-Don, Rostov Oblast, Russia, 344058",
      "local_address": "ул. Тружеников, д.13, Ростов-на-Дону, Ростовская обл., Россия, 344058",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "c6cd5bc3-a20a-40bc-b5a4-6c2abdac2188",
      "coordinates": {"lat": 56.32840527582303, "lon": 38.18285510506781},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "тестовая точка - UPD 3",
      "english_name": "",
      "local_name": "",
      "russian_address": "Боголюбская ул., 1, Сергиев Посад, Московская обл., Россия, 141303",
      "english_address": "Bogolyubskaya Ulitsa, 1, Sergiev Posad, Moskovskaya oblast', Russia, 141303",
      "local_address": "Боголюбская ул., 1, Сергиев Посад, Московская обл., Россия, 141303",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": false
    }, {
      "uuid": "e44cd802-d646-48a7-831d-a39be4ab9946",
      "coordinates": {"lat": 55.778106231679374, "lon": 37.3811279537299},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "Dev",
      "english_name": "",
      "local_name": "",
      "russian_address": "ул. Диснейлендовская, Москва, Россия",
      "english_address": "Ul. Disneylendovskaya, Moskva, Russia",
      "local_address": "ул. Диснейлендовская, Москва, Россия",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 12},
      "is_active": true
    }, {
      "uuid": "9669304c-e363-416a-a355-e5c54391fefa",
      "coordinates": {"lat": 55.208894431802335, "lon": 34.28833007812501},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "ул. Лейтенанта Шмидта, 12, Вязьма, Смоленская обл., Россия, 215110",
      "english_address": "Ulitsa Leytenanta Shmidta, 12, Vyazma, Smolenskaya oblast', Russia, 215110",
      "local_address": "ул. Лейтенанта Шмидта, 12, Вязьма, Смоленская обл., Россия, 215110",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 6},
      "is_active": true
    }, {
      "uuid": "7eff78e1-d914-4466-ad64-826892ed8ad8",
      "coordinates": {"lat": 55.79662982710377, "lon": 37.57038150692466},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "Бебелевская",
      "english_name": "",
      "local_name": "",
      "russian_address": "2-я Бебеля ул., 38 корпус 3, Москва, Россия, 127220",
      "english_address": "2-Ya Bebelya Ulitsa, 38 корпус 3, Moskva, Russia, 127220",
      "local_address": "2-я Бебеля ул., 38 корпус 3, Москва, Россия, 127220",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "36c05350-d0e7-42f4-a435-ce1a6f2c0499",
      "coordinates": {"lat": 55.7532196, "lon": 37.622509},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "ТЕСТ ОБОЗ МОБОЗ",
      "english_name": "",
      "local_name": "",
      "russian_address": "ГУМ (Красная Площадь), Москва, Россия, 109012",
      "english_address": "GUM (Krasnaya Ploschad'), Moskva, Russia, 109012",
      "local_address": "ГУМ (Красная Площадь), Москва, Россия, 109012",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "63641fc7-d131-4b9c-beeb-ce5a25831d88",
      "coordinates": {"lat": 55.75581741, "lon": 37.61764526},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "Тест Анар",
      "english_name": "",
      "local_name": "",
      "russian_address": "пр-д Воскресенские Ворота, 1А, Москва, Россия, 109012",
      "english_address": "Proyezd Voskresenskiye Vorota, 1А, Moskva, Russia, 109012",
      "local_address": "пр-д Воскресенские Ворота, 1А, Москва, Россия, 109012",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "5189bfd3-cf2e-48fc-a47c-8588ded0df00",
      "coordinates": {"lat": 55.75581741, "lon": 37.61764526},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "пр-д Воскресенские Ворота, 1А, Москва, Россия, 109012",
      "english_address": "Proyezd Voskresenskiye Vorota, 1А, Moskva, Russia, 109012",
      "local_address": "пр-д Воскресенские Ворота, 1А, Москва, Россия, 109012",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "425b9441-469a-4f46-81b1-82725328c432",
      "coordinates": {"lat": 56.00006450711856, "lon": 37.22448714312466},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "221",
      "english_name": "",
      "local_name": "",
      "russian_address": "ул. Юности, 8, Зеленоград, Москва, Россия, 124482",
      "english_address": "Ulitsa Yunosti, 8, Zelenograd, Moskva, Russia, 124482",
      "local_address": "ул. Юности, 8, Зеленоград, Москва, Россия, 124482",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "a49b68e0-cf09-45f6-bdf1-eed545878be2",
      "coordinates": {"lat": 39.9901111, "lon": -75.1304527},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "1224",
      "english_name": "",
      "local_name": "",
      "russian_address": "Филадельфия, Пенсильвания 19125, США",
      "english_address": "Philadelphia, PA 19125, USA",
      "local_address": "Philadelphia, PA 19125, USA",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "0b106c0a-e515-451b-a1c0-4f5b20ecb147",
      "coordinates": {"lat": 35.5782843, "lon": -82.54888},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Эшвилл, Северная Каролина 28801, США",
      "english_address": "Asheville, NC 28801, USA",
      "local_address": "Asheville, NC 28801, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "a68ab9e0-4e3d-4fea-868f-7b99737a64b9",
      "coordinates": {"lat": 43.1248133, "lon": -89.3400546},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Мадисон, Висконсин 53704, США",
      "english_address": "Madison, WI 53704, USA",
      "local_address": "Madison, WI 53704, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "c5643f6c-e8b5-4eec-a8ff-a35669763baa",
      "coordinates": {"lat": 37.4237591, "lon": -77.4484174},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Richmond, VA 23237, США",
      "english_address": "Richmond, VA 23237, USA",
      "local_address": "Richmond, VA 23237, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "7babee73-9041-400d-95be-a8afca577c8e",
      "coordinates": {"lat": 40.3174271, "lon": -112.3082208},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Tooele, UT, США",
      "english_address": "Tooele, UT, USA",
      "local_address": "Tooele, UT, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "6103b616-7bd2-4790-8c4e-0bc9c655c292",
      "coordinates": {"lat": 55.65275360462568, "lon": 37.55700697512828},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Херсонская ул., 35 строение 2, Москва, Россия, 117246",
      "english_address": "Khersonskaya Ulitsa, 35 строение 2, Moskva, Russia, 117246",
      "local_address": "Херсонская ул., 35 строение 2, Москва, Россия, 117246",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "c3ff43c5-da1a-46a1-9534-61fa78f63ad5",
      "coordinates": {"lat": 55.92135257850092, "lon": 37.491194904527326},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Лихачевское ш., 44, Долгопрудный, Московская обл., Россия, 141701",
      "english_address": "Likhachevskoye Shosse, 44, Dolgoprudny, Moskovskaya oblast', Russia, 141701",
      "local_address": "Лихачевское ш., 44, Долгопрудный, Московская обл., Россия, 141701",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "bda7d7fa-f472-42cc-a14b-a56ecfa802f9",
      "coordinates": {"lat": 49.3558793, "lon": 6.2017262},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "57970 Йус, Франция",
      "english_address": "57970 Yutz, France",
      "local_address": "57970 Yutz, France",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "489a7b76-1847-4958-a57c-99891811d54d",
      "coordinates": {"lat": 47.8222018, "lon": 7.640444800000001},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "79379 Мюльхайм, Германия",
      "english_address": "79379 Müllheim, Germany",
      "local_address": "79379 Müllheim, Deutschland",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "5c67bdb7-0b18-4550-aef2-e617341d35ca",
      "coordinates": {"lat": 40.8193062, "lon": -77.8428005},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "1800 E Park Ave, State College, PA 16801, США",
      "english_address": "1800 E Park Ave, State College, PA 16801, USA",
      "local_address": "1800 E Park Ave, State College, PA 16801, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "4db5465b-44f2-43fc-8406-8a11ba07b48a",
      "coordinates": {"lat": 44.1639383, "lon": -93.9831458},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Манкейто, Миннесота 56001, США",
      "english_address": "Mankato, MN 56001, USA",
      "local_address": "Mankato, MN 56001, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "ad61d641-7edf-4665-8162-6f3d7009172e",
      "coordinates": {"lat": 42.1006693, "lon": -76.8263707},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Элмайра, Нью-Йорк 14905, США",
      "english_address": "Elmira, NY 14905, USA",
      "local_address": "Elmira, NY 14905, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "3066b9f4-b5b3-45da-9df9-3be6e872de20",
      "coordinates": {"lat": 40.9606261, "lon": -79.000981},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "",
      "english_name": "",
      "local_name": "",
      "russian_address": "Punxsutawney, PA 15767, США",
      "english_address": "Punxsutawney, PA 15767, USA",
      "local_address": "Punxsutawney, PA 15767, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "fd3603f5-bc95-404b-9134-81efe40627e0",
      "coordinates": {"lat": 37.57556650000001, "lon": 127.0239114},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "로앤스qwqwe",
      "english_name": "로앤스",
      "local_name": "로앤스",
      "russian_address": "Южная Корея, Seoul, Dongdaemun-gu, Sinseol-dong, 101-6 동화빌딩 로앤스",
      "english_address": "South Korea, Seoul, Dongdaemun-gu, Sinseol-dong, 101-6 동화빌딩 로앤스",
      "local_address": "대한민국 서울특별시 동대문구 신설동 101-6 동화빌딩 로앤스",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "db5875de-8ee7-470f-aec5-c5a85e07e87a",
      "coordinates": {"lat": 37.4579363, "lon": 127.1898934},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "검단산 헬기장",
      "english_name": "검단산 헬기장",
      "local_name": "검단산 헬기장",
      "russian_address": "산99-8 Geombok-ri, Jungbu-myeon, Gwangju-si, Gyeonggi-do, Южная Корея",
      "english_address": "산99-8 Geombok-ri, Jungbu-myeon, Gwangju-si, Gyeonggi-do, South Korea",
      "local_address": "대한민국 경기도 광주시 중부면 검복리 산99-8",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "266724f5-03a0-4134-8f27-6027377f037b",
      "coordinates": {"lat": 40.028794, "lon": 126.192781},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "묘향산 비행장",
      "english_name": "묘향산 비행장",
      "local_name": "묘향산 비행장",
      "russian_address": "Хянсан, Пхёнан-Пукто, Корейская Народно-Демократическая Республика",
      "english_address": "Hyangsan, North Pyongan, North Korea",
      "local_address": "조선민주주의인민공화국 평안 북도 향산읍",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "e1754d1c-d066-4e8b-9359-612537b42e63",
      "coordinates": {"lat": 37.911858, "lon": 127.1454597},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "Wangbangsan kuksabong Helipad",
      "english_name": "왕방산 국사봉 헬기장",
      "local_name": "왕방산 국사봉 헬기장",
      "russian_address": "산3-3 Bulhyeon-dong, Dongducheon, Gyeonggi-do, Южная Корея",
      "english_address": "산3-3 Bulhyeon-dong, Dongducheon, Gyeonggi-do, South Korea",
      "local_address": "대한민국 경기도 동두천시 불현동 산3-3",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "47f5a646-706a-4343-9e16-eacb211e1a2c",
      "coordinates": {"lat": 36.5471287, "lon": 128.7008506},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "안동병원",
      "english_name": "안동병원",
      "local_name": "안동병원",
      "russian_address": "11 Angsil-ro, Gangnam-dong, Andong, Gyeongsangbuk-do, Южная Корея",
      "english_address": "11 Angsil-ro, Gangnam-dong, Andong, Gyeongsangbuk-do, South Korea",
      "local_address": "대한민국 경상북도 안동시 강남동 앙실로 11",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "fe526014-1214-4d11-9191-500be684bf3d",
      "coordinates": {"lat": -5.85, "lon": 13.05},
      "location_type_uuid": "4adf706e-0be2-4383-b680-a73c959b9e2f",
      "russian_name": "ＢＯＭＡ",
      "english_name": "ＢＯＭＡ",
      "local_name": "ＢＯＭＡ",
      "russian_address": "Япония, 〒341-0018 Saitama, Misato, Waseda, 4 Chome−10−8 菱善ビルＮｏ．１",
      "english_address": "Japan, 〒341-0018 Saitama, Misato, Waseda, 4 Chome−10−8 菱善ビルＮｏ．１",
      "local_address": "日本、〒341-0018 埼玉県三郷市早稲田４丁目１０−８ 菱善ビルＮｏ．１",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "83b29a80-6cb2-45a8-a4e9-f452c4c0e669",
      "coordinates": {"lat": 10.190992404726073, "lon": 42.132641456760076},
      "location_type_uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "russian_name": "የጅጅጋ ኤርፖርትqwqweqweqwe",
      "english_name": "የጅጅጋ ኤርፖርት",
      "local_name": "የጅጅጋ ኤርፖርት",
      "russian_address": "Shinile, ኢትዮጵያ",
      "english_address": "Shinile, Ethiopia",
      "local_address": "Shinile, ኢትዮጵያ",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 3},
      "is_active": true
    }, {
      "uuid": "5642525b-9044-4b2a-ace4-e1b8f346e581",
      "coordinates": {"lat": 9.331636699999999, "lon": 42.9082716},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "የጅጅጋ ኤርፖርት",
      "english_name": "የጅጅጋ ኤርፖርት",
      "local_name": "የጅጅጋ ኤርፖርት",
      "russian_address": "Jijiga, Эфиопия",
      "english_address": "Jijiga, Ethiopia",
      "local_address": "ጅጅጋ, ኢትዮጵያ",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "ec5a9443-2569-4109-9b14-c9ff247c657c",
      "coordinates": {"lat": 27.0898067, "lon": 142.1917474},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "ヘリポート",
      "english_name": "ヘリポート",
      "local_name": "ヘリポート",
      "russian_address": "Oneyama Chichijima, Ogasawara, Tokyo 100-2101, Япония",
      "english_address": "Oneyama Chichijima, Ogasawara, Tokyo 100-2101, Japan",
      "local_address": "日本、〒100-2101 東京都小笠原村父島大根山",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "6c89dbc3-59fe-4c1f-9a5f-5bafca7bc328",
      "coordinates": {"lat": 37.4817399, "lon": 126.8817895},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "제이플라츠111",
      "english_name": "제이플라츠",
      "local_name": "제이플라츠",
      "russian_address": "186 Gasan digital 1-ro, Gasan-dong, Geumcheon-gu, Seoul, Южная Корея",
      "english_address": "186 Gasan digital 1-ro, Gasan-dong, Geumcheon-gu, Seoul, South Korea",
      "local_address": "대한민국 서울특별시 금천구 가산동 가산디지털1로 186",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "67118267-8201-45b1-99be-e7cda46e3d07",
      "coordinates": {"lat": 37.4994051, "lon": 127.0337438},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "포스틸타워",
      "english_name": "포스틸타워",
      "local_name": "포스틸타워",
      "russian_address": "735-3 Yeoksam-dong, Gangnam-gu, Seoul, Южная Корея",
      "english_address": "735-3 Yeoksam-dong, Gangnam-gu, Seoul, South Korea",
      "local_address": "대한민국 서울특별시 강남구 역삼동 735-3",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "374e7fa1-ed58-4ece-b72b-9a42a9655189",
      "coordinates": {"lat": 37.5192524, "lon": 127.0721115},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "잠실한강공원 헬기장",
      "english_name": "잠실한강공원 헬기장",
      "local_name": "잠실한강공원 헬기장",
      "russian_address": "Республика Корея, Seoul, Songpa-gu, Jamsil 2(i)-dong, Sin chun dong 274, 05510",
      "english_address": "South Korea, Seoul, Songpa-gu, Jamsil 2(i)-dong, Sin chun dong 274, 05510",
      "local_address": "대한민국 서울특별시 송파구 잠실2동 Sin chun dong 274, 05510",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "ed67cdcb-e57a-41ab-b24a-0b35cea1c309",
      "coordinates": {"lat": 37.5201132, "lon": 126.8560477},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "목동힐스테이트",
      "english_name": "목동힐스테이트",
      "local_name": "목동힐스테이트",
      "russian_address": "Республика Корея, Seoul, Yangcheon-gu, Sinjeong 1(il)-dong, Jungang-ro 36-gil, 15",
      "english_address": "South Korea, Seoul, Yangcheon-gu, Sinjeong 1(il)-dong, Jungang-ro 36-gil, 15",
      "local_address": "대한민국 서울특별시 양천구 신정1동 중앙로36길 15",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "e1c819c0-8ccd-44a7-998b-b94dbf833c76",
      "coordinates": {"lat": 37.56965599999999, "lon": 126.899058},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "월드컵경기장역123132",
      "english_name": "월드컵경기장역",
      "local_name": "월드컵경기장역",
      "russian_address": "Seongsan 2(i)-dong, Сеул, Южная Корея",
      "english_address": "Seongsan 2(i)-dong, Seoul, South Korea",
      "local_address": "대한민국 서울특별시 성산2동",
      "location_info": null,
      "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
      "is_active": true
    }, {
      "uuid": "386f6c09-c144-4385-aa1a-47bc963520d0",
      "coordinates": {"lat": 35.5166357, "lon": 129.4380112},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "현대중공업그룹",
      "english_name": "현대중공업그룹",
      "local_name": "현대중공업그룹",
      "russian_address": "1000 Bangeojinsunhwan-doro, Jeonha-dong, Dong-gu, Ulsan, Южная Корея",
      "english_address": "1000 Bangeojinsunhwan-doro, Jeonha-dong, Dong-gu, Ulsan, South Korea",
      "local_address": "대한민국 울산광역시 동구 전하동 방어진순환도로 1000",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "362950c7-4387-4a19-8ad6-1077d3aa2903",
      "coordinates": {"lat": 37.5598681, "lon": 126.9673822},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "한국경제신문사 본사",
      "english_name": "한국경제신문사 본사",
      "local_name": "한국경제신문사 본사",
      "russian_address": "463 Cheongpa-ro, Jungnim-dong, Jung-gu, Seoul, Южная Корея",
      "english_address": "463 Cheongpa-ro, Jungnim-dong, Jung-gu, Seoul, South Korea",
      "local_address": "대한민국 서울특별시 중구 중림동 청파로 463",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "813a0398-2716-4e48-83b2-a2d7893cf4b2",
      "coordinates": {"lat": 37.5165432, "lon": 127.1039864},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "한신코아오피스텔",
      "english_name": "한신코아오피스텔",
      "local_name": "한신코아오피스텔",
      "russian_address": "11-9 Sincheon-dong, Songpa-gu, Seoul, Южная Корея",
      "english_address": "11-9 Sincheon-dong, Songpa-gu, Seoul, South Korea",
      "local_address": "대한민국 서울특별시 송파구 신천동 11-9",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "075c62b6-27d9-41e7-9e74-5a19f8d993f9",
      "coordinates": {"lat": 37.5624966, "lon": 126.9978276},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "남산센트럴자이아파트",
      "english_name": "남산센트럴자이아파트",
      "local_name": "남산센트럴자이아파트",
      "russian_address": "235 Toegye-ro, Gwanghui-dong, Jung-gu, Seoul, Южная Корея",
      "english_address": "235 Toegye-ro, Gwanghui-dong, Jung-gu, Seoul, South Korea",
      "local_address": "대한민국 서울특별시 중구 광희동 퇴계로 235",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "a3090098-4a3f-4f24-9703-7e4898c1fd4b",
      "coordinates": {"lat": 37.89180289999999, "lon": 126.788287},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "장산전망대앞헬기착륙장",
      "english_name": "장산전망대앞헬기착륙장",
      "local_name": "장산전망대앞헬기착륙장",
      "russian_address": "산4-6 Imjin-ri, Munsan-eup, Paju-si, Gyeonggi-do, Южная Корея",
      "english_address": "산4-6 Imjin-ri, Munsan-eup, Paju-si, Gyeonggi-do, South Korea",
      "local_address": "대한민국 경기도 파주시 문산읍 임진리 산4-6",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "b18947dd-19d3-42b9-befe-dcfb05b9262b",
      "coordinates": {"lat": 14.1653933, "lon": 101.3391527},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "สนามบินเล็ก มจพ. ปราจีนบุรี",
      "english_name": "สนามบินเล็ก มจพ. ปราจีนบุรี",
      "local_name": "สนามบินเล็ก มจพ. ปราจีนบุรี",
      "russian_address": "Noen Hom, Mueang Prachinburi District, Prachin Buri 25230, Таиланд",
      "english_address": "Noen Hom, Mueang Prachinburi District, Prachin Buri 25230, Thailand",
      "local_address": "ตำบล เนินหอม อำเภอเมืองปราจีนบุรี ปราจีนบุรี 25230 ประเทศไทย",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "a849348c-15ff-4ebb-9875-1e0e14a2ab50",
      "coordinates": {"lat": 37.4015226, "lon": 126.7186661},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "논현한화꿈에그린실거래가격",
      "english_name": "논현한화꿈에그린실거래가격",
      "local_name": "논현한화꿈에그린실거래가격",
      "russian_address": "634-1 Nonhyeon-dong, Namdong-gu, Incheon, Южная Корея",
      "english_address": "634-1 Nonhyeon-dong, Namdong-gu, Incheon, South Korea",
      "local_address": "대한민국 인천광역시 남동구 논현동 634-1",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "dfd330dc-19ad-4f17-b885-002936afeeca",
      "coordinates": {"lat": 19.5052912, "lon": 100.2840118},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "สนามบินเชียงคำ",
      "english_name": "สนามบินเชียงคำ",
      "local_name": "สนามบินเชียงคำ",
      "russian_address": "Chiang Ban, Chiang Kham District, Phayao 56110, Таиланд",
      "english_address": "Chiang Ban, Chiang Kham District, Phayao 56110, Thailand",
      "local_address": "ตำบล เชียงบาน อำเภอ เชียงคำ พะเยา 56110 ประเทศไทย",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "8f92e60f-970c-4a7c-995e-2e75bc94376e",
      "coordinates": {"lat": 19.2938693, "lon": 100.857245},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "สนามบินเชียงกลาง",
      "english_name": "สนามบินเชียงกลาง",
      "local_name": "สนามบินเชียงกลาง",
      "russian_address": "Chiang Klang, Chiang Klang District, Nan 55160, Таиланд",
      "english_address": "Chiang Klang, Chiang Klang District, Nan 55160, Thailand",
      "local_address": "Chiang Klang, Chiang Klang District, Nan 55160, ໄທ",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "c05c8cba-1fe3-44d3-8954-dc40dcbebe50",
      "coordinates": {"lat": 13.3912957, "lon": 101.3199255},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "สนามบินเกาะจันทร์",
      "english_name": "สนามบินเกาะจันทร์",
      "local_name": "สนามบินเกาะจันทร์",
      "russian_address": "Tha Bunmi, Ko Chan District, Chon Buri 20240, Таиланд",
      "english_address": "Tha Bunmi, Ko Chan District, Chon Buri 20240, Thailand",
      "local_address": "ตำบล ท่าบุญมี อำเภอ เกาะจันทร์ ชลบุรี 20240 ประเทศไทย",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }, {
      "uuid": "b90edba6-3b3c-4fd0-a6bd-e7d7836df04f",
      "coordinates": {"lat": 29.8748969, "lon": -103.6971266},
      "location_type_uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "russian_name": "Аэропорт 02 Ранч",
      "english_name": "02 Ranch Airport",
      "local_name": "02 Ranch Airport",
      "russian_address": "Alpine, TX, США",
      "english_address": "Alpine, TX, USA",
      "local_address": "Alpine, TX, USA",
      "location_info": null,
      "location_radius": null,
      "is_active": true
    }], "page": 0, "size": 50, "total_elements": 60172, "total_pages": 1204, "number_of_elements": 50
  },
  "location_types": {
    "content": [{
      "uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
      "infrastructure_facility": true,
      "data_completion": "Автоматически",
      "name": "Аэропорт",
      "description": "Аэропорт",
      "is_active": true
    }, {
      "uuid": "4adf706e-0be2-4383-b680-a73c959b9e2c",
      "infrastructure_facility": true,
      "data_completion": "Автоматически",
      "name": "ЖД станция",
      "description": "ЖД станция",
      "is_active": true
    }, {
      "uuid": "4adf706e-0be2-4383-b680-a73c959b9e4c",
      "infrastructure_facility": true,
      "data_completion": "Автоматически",
      "name": "Контейнерный порт",
      "description": "Контейнерный терминал",
      "is_active": false
    }, {
      "uuid": "4adf706e-0be2-4383-b680-a73c959b9e2f",
      "infrastructure_facility": true,
      "data_completion": "Автоматически",
      "name": "Морской Порт",
      "description": "Морской Порт",
      "is_active": true
    }, {
      "uuid": "5919a99f-d840-4855-b158-5ba957076603",
      "infrastructure_facility": false,
      "data_completion": "Вручную",
      "name": "Опорная точка",
      "description": "Опорная точка1",
      "is_active": true
    }, {
      "uuid": "a2166b4d-27a9-41ce-9fc8-8bfd21baa8d7",
      "infrastructure_facility": true,
      "data_completion": "Автоматически",
      "name": "Пограничный переход",
      "description": "Пограничный переход",
      "is_active": true
    }, {
      "uuid": "4adf706e-0be2-4383-b680-a73c959b9e3c",
      "infrastructure_facility": true,
      "data_completion": "Автоматически",
      "name": "Пункт таможенного оформления/СВХ",
      "description": "Пункт таможенного оформления/СВХ",
      "is_active": true
    }], "page": 0, "size": 100, "total_elements": 7, "total_pages": 1, "number_of_elements": 7
  },
  "location_detail": {
    "uuid": "149aaf0d-2e5e-48af-98f4-36855655e68a",
    "coordinates": {"lat": 47.2072741, "lon": 39.6476128},
    "location_type_uuid": "4adf706e-0be2-4383-b680-a73c959b9e3c",
    "russian_name": "",
    "english_name": "",
    "local_name": "",
    "russian_address": "ул. Тружеников, д.13, Ростов-на-Дону, Ростовская обл., Россия, 344058",
    "english_address": "Ulitsa Truzhenikov, д.13, Rostov-on-Don, Rostov Oblast, Russia, 344058",
    "local_address": "ул. Тружеников, д.13, Ростов-на-Дону, Ростовская обл., Россия, 344058",
    "location_info": null,
    "location_radius": {"vehicle_arrival_radius": 0, "vehicle_departure_radius": 0, "zero_interrogation_radius": 0},
    "is_active": true
  },
  "location_type_detail": {
    "uuid": "bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4",
    "infrastructure_facility": true,
    "data_completion": "Автоматически",
    "name": "Аэропорт",
    "description": "Аэропорт",
    "is_active": true
  }
}
