import RestClient from './RestClient'

const Client = new RestClient()

export default {
  getTasksCount () {
    return Client.get('/oboz2-security-monitor-task-crud/v1/count')
  },
  getInfo () {
    return Client.get('/oboz2-domains-user-info/v1/userInfo')
  }
}
