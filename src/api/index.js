import user from './user'
import geocoder from './geocoder'
import locations from './locations'
import constructor from './constructor'
import table from './table'
import files from './files'

export default {
  user,
  geocoder,
  locations,
  constructor,
  table,
  files
}
