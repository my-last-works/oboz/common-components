/* eslint-disable */
import RestClient from './RestClient'
import { uuid as vue_uuid } from 'vue-uuid'

let Client = new RestClient()

export default {
  getRoute (points) {
    const data = {
      type: 'route.request',
      route_uuid: vue_uuid.v4(),
      base_points: points
    }
    return Client.post(`/oboz2-geo-routing-proxy/v1`, data)
  },
  async getAddressByCoordinates ({ coordinates = [], lang = 'RU' }) {
    try {
      if (window.isMock) {
        return Promise.resolve(data.address_by_coordinates)
      } else {
        let res = await Client.get(
          `/oboz2-geo-geocoding-proxy/v1/get_place_by_coordinates/`,
          {
            params: { coordinates, lang }
          }
        )
        return Promise.resolve((res && res.data) || null)
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  async getCoordinatesByAddress ({ address = '', lang = 'RU' }) {
    try {
      if (window.isMock) {
        return Promise.resolve(data.coordinates_by_address)
      } else {
        let res = await Client.get(
          `/oboz2-geo-geocoding-proxy/v1/get_coordinates_by_address`,
          {
            params: { address, lang }
          }
        )
        return Promise.resolve((res && res.data) || null)
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  async searchAddress ({ search_address = '', lang = 'RU' }) {
    try {
      if (window.isMock) {
        return Promise.resolve(data.search_address_results_list.results)
      } else {
        let res = await Client.get(
          `/oboz2-geo-geocoding-proxy/v1/search_address`,
          {
            params: { search_address, lang }
          }
        )
        return Promise.resolve((res && res.data && res.data.results) || [])
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  async searchAddressSuggester ({ search_address = '', lang = 'RU' }) {
    try {
      if (window.isMock) {
        return Promise.resolve(data.search_address_results_list.results)
      } else {
        // let res = await Client.get(`/oboz2-order-address-suggester/v2/search_address`, {
        let res = await Client.get(`/oboz2-order-address-suggester/v1/search_address`, {
          params: { search_address, lang }
        })
        return Promise.resolve((res && res.data) || [])
      }
    } catch (e) {
      throw new Error(e)
    }
  }
}

const data = {
  search_address_results_list: {
    results: [
      {
        russian_address: 'Вятская улица, 13, Самара, Россия',
        english_address:
          "Vyatskaya Ulitsa, 13, Samara, Samarskaya oblast', Russia, 443022",
        local_address:
          'Вятская ул., 13, Самара, Самарская обл., Россия, 443022',
        coordinates: [53.20039749, 50.24769211]
      },
      {
        russian_address: 'Вятская улица, 13, Ростов-на-Дону, Россия',
        english_address:
          "Ulitsa Vyatskaya, 13, Rostov, Rostovskaya oblast', Russia, 344056",
        local_address:
          'ул. Вятская, 13, Ростов-на-Дону, Ростовская обл., Россия, 344056',
        coordinates: [47.28744507, 39.73517227]
      },
      {
        russian_address:
          'Вятская улица, 13, поселок городского типа Кугеси, Чебоксарский район, Чувашская Республика, Россия',
        english_address:
          'Vostochnaya Ulitsa, 2, Kugesi, Chuvashia, Russia, 429500',
        local_address:
          'Восточная ул., 2, Кугеси, Чувашская Респ., Россия, 429500',
        coordinates: [56.03987885, 47.32597351]
      },
      {
        russian_address: 'Вятская улица, 13, Харьков, Украина',
        english_address:
          "Vyatska St, 13, Kharkiv, Kharkivs'ka oblast, Ukraine, 61000",
        local_address:
          'вулиця Вятська, 13, Харків, Харківська область, Україна, 61000',
        coordinates: [49.99411774, 36.27271652]
      },
      {
        russian_address:
          'Вятская улица, 13, Ижевск, Удмуртская Республика, Россия',
        english_address:
          'Vyatskaya Ulitsa, Izhevsk, Udmurtskaja Respublika, Russia, 426030',
        local_address:
          'Вятская ул., Ижевск, республика Удмуртия, Россия, 426030',
        coordinates: [56.93481445, 53.22868729]
      },
      {
        russian_address: 'Вятская улица, 13А, Алматы, Казахстан',
        english_address: 'Vyatskaya Ulitsa, Almaty, Kazakhstan',
        local_address: 'Вятская улица, Алматы, Қазақстан',
        coordinates: [43.31915283, 76.94187164]
      },
      {
        russian_address: 'Вятская улица, 13, слобода Заречная, Киров, Россия',
        english_address:
          "Vyatskaya Ulitsa, 13, Kirov, Kirovskaya oblast', Russia, 610018",
        local_address: 'Вятская ул., 13, Киров, Кировская обл., Россия, 610018',
        coordinates: [58.60676956, 49.74969101]
      }
    ],
    empty: false
  },
  address_by_coordinates: {
    russian_address:
      'Россия, Московская область, городской округ Лотошино, сельское поселение Микулинское',
    english_address: "Unnamed Road, Moskovskaya oblast', Russia",
    local_address: 'Unnamed Road, Московская обл., Россия',
    coordinates: [56.36408990854546, 35.74426801034348]
  },
  coordinates_by_address: {}
}
