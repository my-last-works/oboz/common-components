import cloneDeep from 'clone-deep'

export const formatNumber = (val, { dot = 0, signed = false, zero = false, separator = null } = {}) => {
  if (isNaN(val) || !val) {
    val = 0
  }
  if (zero && +(+val).toFixed(dot) === 0) {
    return zero
  }

  let result = (signed && val > 0 ? '+' : '') + (+(+val).toFixed(dot)).toLocaleString('ru', { minimumFractionDigits: dot })
  if (separator) {
    result = result.replace(',', separator)
  }

  if (val && typeof val === 'string' && val.match(/\.$/g) && !dot) {
    result = result + '.'
  }

  return result
}

export const generateFormData = ({ data = {}, parentData, model, assign = true, toService = false }) => {
  if (!model) return data
  let generate = (data, localModel) => {
    let result = {}
    localModel = cloneDeep(localModel)
    data = cloneDeep(data || {})

    Object.keys(localModel).forEach((key) => {
      let modelValue = localModel[key].value && typeof localModel[key].value === 'function' ? localModel[key].value(data, parentData) : localModel[key].value
      let isExclude = localModel[key].exclude && typeof localModel[key].exclude === 'function' ? localModel[key].exclude(data, parentData) : localModel[key].exclude
      let dataValue = data && data[key]

      if (dataValue && localModel[key].type) {
        switch (localModel[key].type) {
          case 'number':
            dataValue = Number(dataValue)
            break
        }
      }

      if (isExclude && toService) return

      if (localModel[key].nested_fields) {
        if (localModel[key].transform && toService) {
          let { byField, suffix } = localModel[key].transform

          suffix = suffix === false ? false : byField
          key = `${key}${suffix ? '_' + suffix : ''}`
          result[key] = dataValue[byField]
        } else {
          result[key] = generate(data && data[key], localModel[key].nested_fields)
        }
      } else if (localModel[key].validator && localModel[key].validator.$each) {
        result[key] = assign && data && data[key] && data[key].length
          ? data[key].map(item => generate(item, localModel[key].validator.$each))
          : modelValue
      } else {
        result[key] = assign && dataValue !== undefined ? dataValue : modelValue
      }
    })
    return result
  }

  if (Array.isArray(data)) {
    return data.map(generate)
  } else {
    return generate(data, model)
  }
}

export const getValueFromPath = ({ data, path, processEach = true, unbind = false }) => {
  if (!path) return undefined
  let pathArr = path.split('.').filter(item => Boolean(item))
  let result
  let localData = data

  pathArr.forEach((key, index) => {
    if (typeof result !== 'object' && index !== 0) return

    result = localData[key]

    if (result && result.$each && processEach) {
      result = Object.keys(result.$each).filter(key => /^\d+$/.test(key)).map((key) => {
        return { ...result.$each[key] }
      })
    } else if (result && result.nested_fields) {
      result = result.nested_fields
    }

    localData = result
  })

  result = (result && result.validator) || result

  return unbind ? cloneDeep(result) : result
}

export const sortArray = (arr, param) => {
  if (arr && arr.sort) {
    if (typeof param === 'string') {
      arr.sort((i, ii) => {
        if (i[param] > ii[param]) {
          return 1
        } else if (i[param] < ii[param]) {
          return -1
        } else {
          return 0
        }
      })
    } else {
      arr.sort()
    }
    return arr
  } else {
    return false
  }
}
