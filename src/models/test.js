import { minLength, required, requiredIf } from 'vuelidate/lib/validators'

const TEST = {
  title: {
    value: '',
    validator: {
      required: {
        method: requiredIf(function (model) {
          return model && model.type === false
        }),
        error: 'Вы выбрали неверный тип, получите ошибку'
      }
    }
  },
  type: {
    value: null,
    validator: {
      required
    }
  },
  description: {
    value: '',
    validator: {
      required,
      minLength: {
        method: minLength(5),
        error: 'Минимум положено 5 символов'
      }
    }
  },
  status: {
    nested_fields: {
      text: {
        value: '',
        validator: {
          required,
          minLength: {
            method: minLength(5),
            error: 'Нужно 10 буков'
          }
        }
      }
    }
  },
  notes: {
    type: 'array',
    value: [],
    validator: {
      required: {
        method: requiredIf(function () {
          return this.formData.type === true
        }),
        error: 'Добавьте заметку'
      },
      $each: {
        title: {
          value: '',
          validator: {
            required
          }
        }
      }
    }
  }
}

export default {
  TEST
}
