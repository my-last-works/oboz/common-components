export default {
  methods: {
    getTableCellHtml (item, column) {
      if (column.html) {
        return this.markWithSearchQuery(column.html(item))
      } else {
        if (item.hasOwnProperty(column.key)) {
          return this.markWithSearchQuery(item[column.key])
        } else {
          return '<div class="missing-property">missing property</div>'
        }
      }
    },
    markWithSearchQuery (cellHtml) {
      // Not an empty string, false, null etc
      if (typeof cellHtml !== 'object' && typeof cellHtml !== 'undefined') {
        cellHtml = cellHtml.toString()

        const cleanString = cellHtml.replace(/<[^>]*>?/gm, '').replace(/\s\s+/g, ' ').trim()

        let reg = `(${this.search})`
        let regex = new RegExp(reg, 'i')

        if (!cleanString.match(regex)) {
          return cellHtml
        }

        let matchStartPosition = cleanString.match(regex).index
        let matchEndPosition = matchStartPosition + cleanString.match(regex)[0].toString().length
        let originalTextFoundByRegex = cleanString.substring(matchStartPosition, matchEndPosition)

        cellHtml = cellHtml.replace(regex, `<mark>${originalTextFoundByRegex}</mark>`)
      }

      return cellHtml
    }
  }
}
