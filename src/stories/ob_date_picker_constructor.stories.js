/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, select, object } from '@storybook/addon-knobs'

import ObDatepicker from '../obComponents/obDatePickerConstructor/index'
import ObRadio from '../obComponents/obRadio/index.vue'
import { withInfo } from 'storybook-addon-vue-info'

const defaultSinceTo = [
  {
    from: '2019-12-24',
    to: '2019-12-28 23:59'
  },
  {
    from: '2019-12-15',
    to: '2019-12-20 23:59'
  }
]

storiesOf('ObDatepickerConstructor', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObDatepicker, ObRadio },
      props: {
        format: {
          type: String,
          default: text('format', 'dd.mm.yyyy')
        },
        placeholder: {
          type: String,
          default: text('placeholder', 'Date')
        },
        label: {
          type: String,
          default: text('label', 'Date')
        },
        radioModel: {
          type: String,
          default: text('radioModel', '0')
        },
        position: {
          default: text('position', 'bottom-start')
        },
        sinceTo: {
          type: Array,
          default: object('sinceTo', defaultSinceTo)
        },
        blockAll: {
          type: Boolean,
          default: boolean('blockAll', false)
        },
        blockBefore: {
          type: Boolean,
          default: boolean('blockBefore', false)
        },
        blockTimeBefore: {
          type: String,
          default: text('time from which we can select', '20:00')
        },
        allowHolidays: {
          type: Boolean,
          default: boolean('allowHolidays', false)
        },
        allowWeekends: {
          type: Boolean,
          default: boolean('allowWeekends', false)
        },
        allowAll: {
          type: Boolean,
          default: boolean('allowAll', false)
        },
        lang: {
          default: select('lang', ['ru', 'en', 'zh'], 'ru')
        },
        isInline: {
          default: boolean('isInline', false)
        },
        isTime: {
          default: boolean('isTime', true)
        },
        readonly: {
          default: boolean('readonly', false)
        },
        required: {
          default: boolean('required', false)
        },
        disabled: {
          default: boolean('disabled', false)
        }
      },
      data () {
        return {
          date: new Date()
        }
      },
      template: `
        <div class="element_container">
          <ObDatepicker
            :format="format"
            :placeholder="placeholder"
            :label="label"
            :blockAll="blockAll"
            :blockBefore="blockBefore"
            :blockTimeBefore="blockTimeBefore"
            :sinceTo="sinceTo"
            :allowHolidays="allowHolidays"
            :allowWeekends="allowWeekends"
            :allowAll="allowAll"
            :lang="lang"
            :is-inline="isInline"
            :is-time="isTime"
            :required="required"
            :disabled="disabled"
            :readonly="readonly"
            v-model="date"
          >
            <div style="display:flex; justify-content: space-around">
              <ObRadio
                label="Погрузка"
                valueInput="0"
                v-model="radioModel"
              />
              
              <ObRadio
                label="Отгрузка"
                valueInput="1"
                v-model="radioModel"
              />
            </div>
          </ObDatepicker>
        </div>`
    }
  },
  {
    info: {
      components: { ObDatepicker },
      summary: `
      \`\`\`html
        <ObDatepicker
            :format="format"
            :placeholder="placeholder"
            :label="label"
            :blockAll="blockAll"
            :blockBefore="blockBefore"
            :blockTimeBefore="blockTimeBefore"
            :sinceTo="sinceTo"
            :allowHolidays="allowHolidays"
            :allowWeekends="allowWeekends"
            :allowAll="allowAll"
            :lang="lang"
            :is-inline="isInline"
            :is-time="isTime"
            :required="required"
            :disabled="disabled"
            :readonly="readonly"
            v-model="date"
          >
            <div style="display:flex; justify-content: space-around">
              <ObRadio
                label="Погрузка"
                valueInput="0"
                v-model="radioModel"
              />
              
              <ObRadio
                label="Отгрузка"
                valueInput="1"
                v-model="radioModel"
              />
            </div>
          </ObDatepicker>
      \`\`\`
      `,
      source: false
    }
  })
