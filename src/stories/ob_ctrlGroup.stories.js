/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, object, select } from '@storybook/addon-knobs'

import ObControlGroup from '../obComponents/obControlGroup'
import ObSelector from '../obComponents/obSelect'
import { withInfo } from 'storybook-addon-vue-info'

storiesOf('ObControlGroup', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObControlGroup, ObSelector },
      props: {
        value: {
          type: [String, Array]
        },

        items: {
          type: Array,
          default: object('Items to show', [
            {
              label: 'Целые машины',
              value: 'Целые машины',
              icon: 'info',
              tooltipIcon: 'auction'
            },
            {
              label: 'Контейнеры',
              value: 'Контейнеры',
              tooltip: 'Отслеживание передвижение заказа с помощью специальных приборов - “пломб”, устанавливаемых на транспортные средства'
            },
            {
              label: 'Вагоны',
              value: 'Вагоны'
            },
            {
              label: 'Авиаперевозки',
              value: 'Авиаперевозки',
              tooltip: 'Отслеживание передвижение заказа с помощью специальных приборов - “пломб”, устанавливаемых на транспортные средства'
            },
            {
              label: 'Морские перевозки',
              value: 'Морские перевозки'
            }
          ])
        },

        activeClass: {
          type: String,
          default: text('Active class', 'active')
        },

        multiselect: {
          type: Boolean,
          default: boolean('Multiselect', false)
        },
        disabled: {
          type: Boolean,
          default: boolean('disabled', false)
        },
        theme: {
          type: String,
          default: select('theme', [ 'default', 'light' ], 'default')
        },
        height: {
          type: String,
          default: select('Height', [ 'default', 'slim' ], 'default')
        },
        tooltipIcon: {
          type: String,
          default: text('tooltipIcon', 'info')
        }
      },

      data () {
        return {
          ctrlGroup: 'Вагоны',
          formData: {
            name: '',
            title: ''
          },
          ctrlGroupMulti: ['Вагоны', 'Авиаперевозки']
        }
      },

      template: `
        <div style='margin: 20px;'>
          <h2 style='margin: 20px;'>Default options</h2>
          <ObControlGroup
            v-model="ctrlGroup"
            :items="items"
            :activeClass="activeClass"
            :multiselect="multiselect"
            :disabled="disabled"
            :theme="theme"
            :height="height"
            :tooltipIcon="tooltipIcon"
          />
          <h2 style='margin: 20px;'>Multiselect</h2>
          <ObControlGroup
            v-model="ctrlGroupMulti"
            :items="items"
            :activeClass="activeClass"
            :multiselect="true"
            :disabled="disabled"
            :theme="theme"
            :height="height"
            :tooltipIcon="tooltipIcon"
          />
          <h2 style='margin: 20px;'>With <code>slot name="extra"</code></h2>
          <ObControlGroup
            v-model="ctrlGroup"
            :items="items"
            :activeClass="activeClass"
            :multiselect="multiselect"
            :disabled="disabled"
            :theme="theme"
            :height="height"
            :tooltipIcon="tooltipIcon"
          >
          <template v-slot:extra>
            <ObSelector
              v-model="formData.title"
              label="Страны 123"
              :show-items-count="4"
              :is-multiselect="true"
              :is-auto-complete="true"
              :disabled="disabled"
              :theme="theme"
              :tooltipIcon="tooltipIcon"
            />
          </template>
          </ObControlGroup>
        </div>`
    }
  },
  {
    info: {
      components: { ObControlGroup },
      summary: `
      \`\`\`html
        <ObControlGroup
          v-model="ctrlGroup"
          :items="items"
          :activeClass="activeClass"
          :multiselect="multiselect"
          :disabled="disabled"
          :theme="theme"
          :height="height"
          :tooltipIcon="tooltipIcon"
        />
      \`\`\`
      `,
      source: false
    }
  })
