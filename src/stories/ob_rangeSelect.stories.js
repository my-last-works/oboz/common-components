/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, object, number } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'

import ObRangeSelect from '../obComponents/obRangeSelect/obRangeSelect'

storiesOf('ObRangeSelect', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObRangeSelect },
      props: {
        value: {
          type: [Number, Object],
          default: number('value', 40)
        },

        startVal: {
          type: Number,
          default: number('Start Value', 0)
        },

        endVal: {
          type: Number,
          default: number('End value', 100)
        },

        units: {
          type: [Array, String],
          default: object('Units', [{ label: 'Лк', uuid: 12 }, { label: 'фот', uuid: 34 }])
        },

        icon: {
          type: Object,
          default: object('Icon', { name: 'pin-filled', color: 'purple' })
        },

        label: {
          type: String,
          default: text('Label', 'Минимальный уровень освещения')
        },

        isDisabled: {
          type: Boolean,
          default: boolean('isDisabled', false)
        },

        step: {
          type: Number,
          default: number('Range Step', 1)
        },

        color: {
          type: String,
          default: text('Color', 'blue')
        }
      },

      data () {
        return {
          range: this.value
        }
      },

      template: `
        <div class="element_container">
          <ObRangeSelect
            v-model="range"
            :startVal="startVal"
            :endVal="endVal"
            :units="units"
            :icon="icon"
            :label="label"
            :isDisabled="isDisabled"
            :step="step"
            :color="color"
          />
        </div>
        `,

      description: {
        ObRangeSelect: {
          props: {
            value: 'Just a part of v-model',
            startVal: 'Startng value',
            endVal: 'Ending value',
            units: 'Measurement units',
            icon: 'Icon that shown on the right side',
            label: 'Just a label',
            isDisabled: 'Whether component is disabled or not',
            step: 'Which step used to change value',
            color: 'Color of the selected interval'
          },
          events: {
            input: 'Event for v-model. Fired when you move the range slider or channge measurement units. Return object { "value": number, "unit": string }'
          },
          slots: {
            default: 'Place text or icon here'
          }
        }
      }
    }
  },
  {
    info: {
      summary: `Base range slider`
    }
  }
  )
