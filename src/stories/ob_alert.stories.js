/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObAlert from '../obComponents/obAlert/index.vue'

const stories = storiesOf('ObAlert', module)

stories.addDecorator(withKnobs({
  escapeHTML: false
}))
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    textSmall: {
      default: text('textSmall', 'Текст уведомления')
    },
    textBig: {
      default: text('textBig', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ultricies metus tortor, ut tincidunt turpis placerat eget. Etiam vitae consequat ligula, vitae aliquet sapien. In hac habitasse platea dictumst. Nam tristique condimentum risus, id pulvinar metus vehicula nec. Nullam sit amet sapien pharetra, mattis lacus at, placerat augue. Sed pellentesque mi quis arcu dictum, eu rutrum mi condimentum. Donec a dolor erat. In malesuada venenatis turpis, ac accumsan magna. Cras rutrum faucibus tellus, eu lobortis eros sagittis non. Etiam malesuada, orci eu congue luctus, nulla eros porta ante, in tincidunt tortor risus quis est. Morbi efficitur sagittis ipsum, id tincidunt odio aliquet quis. Curabitur dictum, nibh non volutpat porta, metus leo consectetur velit, nec sagittis mauris metus at dolor. Aliquam quis lectus finibus, cursus leo vel, dictum est. Aenean eros dolor, mollis a eros vel, tempor pulvinar dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi at tincidunt ante.')
    },
    textHtml: {
      default: text('textHtml', 'Вот иконка: <span class="icon checked" style="color: green">')
    },
    icon1: {
      default: text('icon1', 'info')
    },
    icon2: {
      default: text('icon2', 'checked')
    },
    icon3: {
      default: text('icon3', 'alert')
    },
    icon4: {
      default: text('icon4', 'delete')
    },
    themeIcon: {
      default: text('theme-icon', null)
    },
    theme1: {
      default: text('theme-info', 'info')
    },
    theme2: {
      default: text('theme-success', 'success')
    },
    theme3: {
      default: text('theme-warning', 'warning')
    },
    theme4: {
      default: text('theme-danger', 'danger')
    },
    theme5: {
      default: text('theme-info-flat', 'info-flat')
    },
    theme6: {
      default: text('theme-success-flat', 'success-flat')
    },
    theme7: {
      default: text('theme-danger-flat', 'danger-flat')
    },
    hideCloseButton: {
      default: boolean('hideCloseButton', true)
    }
  },
  data () {
    return {
      demoCounter: 0
    }
  },
  components: { ObAlert },
  template: `
    <div style='padding: 20px;'>
      <ObAlert 
        :text="textSmall"
        :theme="theme1"
        :theme-icon="themeIcon"
        :icon="icon1"
      />

      <h4 style="padding: 10px 0 20px;">С таймаутом (5 секунд):</h4>
      
      <ObAlert 
        :text="textSmall"
        :theme="theme1"
        :theme-icon="themeIcon"
        :icon="icon1"
        :timeout="5000"
        :hide-close-button="true"
      />

      <h4 style="padding: 10px 0 20px;">Без иконки:</h4>
      
      <ObAlert 
        :text="textSmall"
        :theme="theme1"
        :theme-icon="themeIcon"
      />
      
      <h4 style="padding: 10px 0 20px;">С большим текстом:</h4>
      
      <ObAlert 
        :theme="theme1"
        :theme-icon="themeIcon"
        :icon="icon1"
      >
        {{ textBig }}
      </ObAlert>
      
      <h4 style="padding: 10px 0 20px;">С HTML кодом:</h4>
      
      <ObAlert 
        :text="textHtml"
        :theme="theme1"
        :theme-icon="themeIcon"
        :icon="icon1"
      />

      <h4 style="padding: 10px 0 20px;">С отключенной кнопкой закрытия:</h4>

      <ObAlert
        :text="textSmall"
        :theme="theme1"
        :theme-icon="themeIcon"
        :icon="icon1"
        :hide-close-button="hideCloseButton"
      />
      
      <h4 style="padding: 10px 0 20px;">Другие цветовые решения:</h4>
      
      <ObAlert 
        :text="textSmall"
        :theme="theme2"
        :theme-icon="themeIcon"
        :icon="icon2"
      />
      
      <ObAlert 
        :text="textSmall"
        :theme="theme3"
        :theme-icon="themeIcon"
        :icon="icon3"
      />
      
      <ObAlert 
        :text="textSmall"
        :theme="theme4"
        :theme-icon="themeIcon"
        :icon="icon4"
      />

      <ObAlert 
        :text="textSmall"
        :theme="theme5"
        :theme-icon="themeIcon"
        :icon="icon1"
      />
      
      <ObAlert 
        :text="textSmall"
        :theme="theme6"
        :theme-icon="themeIcon"
        :icon="icon2"
      />
      
      <ObAlert 
        :text="textSmall"
        :theme="theme7"
        :theme-icon="themeIcon"
        :icon="icon4"
      />
    </div>`
}),
{
  info: {
    components: { ObAlert },
    summary: `
        \`\`\`html
        <ObAlert 
          :text="textSmall"
          :theme="theme7"
          :theme-icon="themeIcon"
          :icon="icon4"
        />
        \`\`\`
      `,
    source: false
  }
})
