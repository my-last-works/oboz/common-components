/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ObButton from '../obComponents/obButton'
import ObSelector from '../obComponents/obSelect/index.vue'
import { withInfo } from 'storybook-addon-vue-info'

storiesOf('ObControlsGroup', module)
  .addDecorator(withInfo)
  .add('sample', () => ({
    data () {
      return {
        val: '',
        items: [
          {
            title: 'Title 1',
            uuid: 'Value 1',
            icon: 'alert'
          },
          {
            title: 'Title 2',
            uuid: 'Value 2',
            icon: 'alert'
          },
          {
            title: 'Title 3',
            uuid: 'Value 2',
            icon: 'alert'
          },
          {
            title: 'Title 4',
            uuid: 'Value 2',
            icon: 'alert'
          },
          {
            title: 'Title 5',
            uuid: 'Value 2',
            icon: 'alert'
          }
        ]
      }
    },
    components: { ObButton, ObSelector },
    template: `
      <div class="element_container">
        <div class="ctrl-group">
            <ObButton 
                label="Кнопка Кнопка Кнопка"
                @click="clickHandler"
            />
            <ObButton 
                label="Кнопка"
                @click="clickHandler"
            />
            <ObButton 
                label="Кнопка"
                @click="clickHandler"
            />
            <ObSelector
                :items="items" 
                v-model="val"/>
        </div>
      </div>`,
    methods: {
      clickHandler (e) {
      }
    }
  }),
  {
    info: {
      components: { },
      summary: `
      \`\`\`html
        <div class="ctrl-group">
            <ObButton 
                label="Кнопка Кнопка Кнопка"
                @click="clickHandler"
            />
            <ObButton 
                label="Кнопка"
                @click="clickHandler"
            />
            <ObButton 
                label="Кнопка"
                @click="clickHandler"
            />
            <ObSelector
                :items="items" 
                v-model="val"/>
        </div>
      \`\`\`
      `,
      source: false
    }
  })
