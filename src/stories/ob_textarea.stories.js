/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'

import ObTextarea from '../obComponents/obTextarea/index.vue'
import { withInfo } from 'storybook-addon-vue-info'

const stories = storiesOf('ObTextarea', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)

stories.add('sample', () => ({
  props: {
    label: {
      default: text('label', 'Комментарий')
    },
    placeholder: {
      default: text('placeholder', 'Введите комментарий')
    },
    exactHeight: {
      default: text('exactHeight', '100')
    },
    maxLength: {
      default: text('maxLength', '100')
    },
    disabled: {
      default: boolean('disabled', false)
    },
    required: {
      default: boolean('required', false)
    },
    readonly: {
      default: boolean('readonly', false)
    }
  },
  data () {
    return {
      value: ''
    }
  },
  components: { ObTextarea },
  template: `
    <div class="element_container">
      <h3>Textarea</h3>
      <br>
      <ObTextarea
        :label="label"
        :placeholder="placeholder"
        :disabled="disabled"
        :required="required"
        :readonly="readonly"
        :exactHeight="exactHeight"
        :maxLength="maxLength"
        v-model="value"
      />

      <br><br>
      value: {{ value }}
      <br><br>

    </div>
  `
}),
{
  info: {
    components: { ObTextarea },
    summary: `
      \`\`\`html
        <ObTextarea
          :label="label"
          :placeholder="placeholder"
          :disabled="disabled"
          :required="required"
          :readonly="readonly"
          v-model="value"
        />
      \`\`\`
    `,
    source: false
  }
})
