/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, object } from '@storybook/addon-knobs'

import ObDateTimePickerMobile from '../obComponents/obDateTimePickerMobile/index'
import { withInfo } from 'storybook-addon-vue-info'

const disabledDates = {
  // позволяет ограничить время с периода и по какой (параметр не обязательный)
  to: new Date(),
  from: ''
}

const typesOfShipment = [
  // добавляет подразделы для модального окна (работает с пропсом withTime)
  // по дефолту можно не указывать данный пропс
  {
    label: 'Погрузка',
    value: 'loading',
    disabled: false
  },
  {
    label: 'Отгрузка',
    value: 'shipping',
    disabled: true
  }
]

storiesOf('ObDatepickerMobile', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObDateTimePickerMobile },
      props: {
        format: {
          type: [String, Object, Date]
        },
        label: {
          type: String,
          default: text('label', 'Date')
        },
        placeholder: {
          type: String,
          default: text('placeholder', 'Date')
        },
        icon: {
          type: String,
          default: text('icon', 'calendar')
        },
        required: {
          type: Boolean,
          default: boolean('required', false)
        },
        withTime: {
          type: Boolean,
          default: boolean('with-time', false)
        },
        buttonText: {
          type: String,
          default: text('button-text', 'Выбрать')
        },
        disabledDates: {
          type: Object,
          default: object('disabled-dates', disabledDates)
        },
        typesOfShipment: {
          type: Array,
          default: object('types-of-shipment', typesOfShipment)
        }
      },
      data () {
        return {
          date: new Date()
        }
      },
      template: `
        <div class="element_container">
          <ObDateTimePickerMobile
              :value="format"
              :label="label"
              :placeholder="placeholder"
              :icon="icon"
              :required="required"
              :with-time="withTime"
              :button-text="buttonText"
              :disabled-dates="disabledDates"
              :types-of-shipment="typesOfShipment"
              @setPeriod="setPeriod"
              @input="changePeriod"
          />
        </div>`,
      methods: {
        setPeriod (date) {
          this.format = date
        },
        changePeriod (date) {
          alert('date', date)
        }
      }
    }
  },
  {
    info: {
      summary: `
    See \`Knobs\` tab to inspect drop event. Usage:
    \`\`\`html
      <ObDateTimePickerMobile
              :value="format"
              :label="label"
              :placeholder="placeholder"
              :icon="icon"
              :required="required"
              :with-time="withTime"
              :button-text="buttonText"
              :disabled-dates="disabledDates"
              :types-of-shipment="typesOfShipment"
              @setPeriod="setPeriod"
              @input="changePeriod"
          />
    \`\`\`
  `,
      source: false
    }
  })
