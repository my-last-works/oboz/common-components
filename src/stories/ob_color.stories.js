/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

storiesOf('ObColor', module)
  .add('sample', () => ({
    data () {
      return {
        label: 'Цвета',
        disabled: false,
        dark: false,
        danger: false,
        mini: false,
        type: 'primary',
        demoCounter: 0,
        searchColor: '',
        colors: [
          { class: 'shale-gray', color: 'shale-gray', hex: '#474B5B', group: 'common' },
          { class: 'gray-slate', color: 'gray-slate', hex: '#737682', group: 'common' },
          { class: 'gray-window', color: 'gray-window', hex: '#A3A5AD', group: 'common' },
          { class: 'light-lavender', color: 'light-lavender', hex: '#F2F7FD', group: 'common' },
          { class: 'lavender', color: 'lavender', hex: '#E5EDF9', group: 'common' },
          { class: 'midnight-blue', color: 'midnight-blue', hex: '#012C73', group: 'common' },
          { class: 'cobalt-blue', color: 'cobalt-blue', hex: '#0242AD', group: 'common' },
          { class: 'cornflower', color: 'cornflower', hex: '#6693DE', group: 'common' },
          { class: 'cornflower-craiola', color: 'cornflower-craiola', hex: '#99B7E9', group: 'common' },
          { class: 'blue-blue-hoarfrost', color: 'blue-blue-hoarfrost', hex: '#C5E0FE', group: 'common' },
          { class: 'deep-blue', color: 'deep-blue', hex: '#00CCA6', group: 'common' },
          { class: 'green-craiola', color: 'green-craiola', hex: '#0DA577', group: 'common' },
          { class: 'persian-red', color: 'persian-red', hex: '#C6343F', group: 'common' },
          { class: 'gorse-yellow', color: 'gorse-yellow', hex: '#FFEB48', group: 'common' },
          { class: 'saturated-yellow', color: 'saturated-yellow', hex: '#E7A031', group: 'common' },
          { class: 'diamond-orange-yellow', color: 'diamond-orange-yellow', hex: '#FFB137', group: 'common' },
          { class: 'purple', color: 'purple', hex: '#8968AE', group: 'common' },
          { class: 'amethyst', color: 'amethyst', hex: '#9973C4', group: 'common' },
          // colorsAliases
          { class: 'light-denim', color: 'color-green-dark', hex: '#00B391', group: 'aliases' },
          { class: 'cobalt-black', color: 'color-text-base', hex: '#191E32', group: 'aliases' },
          { class: 'very-pale-blue', color: 'color-grey-border', hex: '#D1D2D6', group: 'aliases' },
          { class: 'light-black', color: 'color-light-grey', hex: '#E8E9EB', group: 'aliases' },
          { class: 'very-light-black', color: 'color-white-lilac', hex: '#F4F4F5', group: 'aliases' },
          { class: 'white', color: 'background-color-light', hex: '#FAFCFE', group: 'aliases' },
          { class: 'blue-dust', color: 'color-blue-smalt', hex: '#0070A6', group: 'aliases' },
          { class: 'cobalt', color: 'color-blue-navy-blue', hex: '#0070A6', group: 'aliases' },
          { class: 'caribbean', color: 'color-green-dark', hex: '#00B391', group: 'aliases' },
          { class: 'caribbean-green', color: 'color-green-light', hex: '#10CF96', group: 'aliases' },
          { class: 'light-raspberry-red', color: 'color-error', hex: '#E62D3B', group: 'aliases' },
          // colorCss
          { class: '-', color: 'cssMine_not_ready_color_after', hex: '#01255d', group: 'css' },
          { class: '-', color: 'cssMine_not_ready_background_before', hex: 'rgba(255, 255, 255, 0.7)', group: 'css' },
          { class: '-', color: 'cssMine_ctrl_group_is_active_box_shadow', hex: 'rgba(0, 0, 0, 0.4)', group: 'css' },
          { class: '-', color: 'cssMine_button_flat-type_box_shadow_hover', hex: 'rgba(0, 0, 0, 0.4)', group: 'css' },
          { class: '-', color: 'cssTableStyle_table_panel_inner_mark_background', hex: '#03a9f4', group: 'css' },
          { class: '-', color: 'cssTableStyle_ob_table_inner_isEdit_background', hex: '#e2e2e2', group: 'css' },
          { class: '-', color: 'cssTableStyle_gradient_color_1', hex: '#f9fafd', group: 'css' },
          { class: '-', color: 'cssTableStyle_gradient_color_2', hex: '#d9e4f7', group: 'css' },
          { class: '-', color: 'cssTableStyle_th_background_hover', hex: '#f2f2f2', group: 'css' },
          { class: '-', color: 'cssTableStyle_table-empty_color', hex: '#9e9e9e', group: 'css' },
          { class: '-', color: 'cssTableStyle_loader_row_background', hex: 'rgba(255, 255, 255, 0.7)', group: 'css' },
          { class: '-', color: 'cssTableStyle_footer_gradient_color_1_before', hex: 'rgba(196, 213, 237, 0.5)', group: 'css' },
          { class: '-', color: 'cssTableStyle_footer_gradient_color_2_before', hex: 'rgba(207, 222, 244, 0)', group: 'css' },
          { class: '-', color: 'cssForm_el_switch_core_background_color_after', hex: '#828D99', group: 'css' },
          { class: '-', color: 'cssForm_el_switch_core_box_shadow_after', hex: 'rgba(13, 13, 13, 0.6)', group: 'css' },
          { class: '-', color: 'cssForm_el_switch_core_box_shadow_after', hex: 'rgba(1, 76, 201, 0.48)', group: 'css' },
          { class: '-', color: 'cssForm_tooltip_box_shadow', hex: 'rgba(0, 0, 0, 0.15)', group: 'css' },
          { class: '-', color: 'cssForm_is_disabled_inner_color', hex: 'rgb(84, 84, 84)', group: 'css' },
          { class: '-', color: 'cssDxSwitch_handle_background_color_before', hex: '#CFD3E1', group: 'css' },
          { class: '-', color: 'cssDxSwitch_box_shadow_before', hex: '#CFD2E1', group: 'css' },
          { class: '-', color: 'cssDxSwitch_on_value_container_box_shadow_before', hex: 'rgba(1, 76, 201, 0.48)', group: 'css' },
          { class: '-', color: 'cssDxSwitch_on_value_handle_box_shadow_before', hex: 'rgba(0, 112, 166, 0.6)', group: 'css' }, // $color-blue-navy-blue
          { class: '-', color: 'cssSwitch_core_box_shadow', hex: '#CFD2E1', group: 'css' },
          { class: '-', color: 'cssSwitch_core_background_after', hex: '#CFD3E1', group: 'css' },
          { class: '-', color: 'cssSwitch_is_checked_core_box_shadow', hex: 'rgba(1, 76, 201, 0.48)', group: 'css' },
          { class: '-', color: 'cssObozFont_razgr_before', hex: 'rgb(176, 198, 231)', group: 'css' },
          { class: '-', color: 'cssObozFont_razgr_after', hex: 'rgb(255, 255, 255)', group: 'css' },
          { class: '-', color: 'cssObozFont_razgr_path3_before', hex: 'rgb(255, 255, 255)', group: 'css' },
          { class: '-', color: 'cssObozFont_razgr_path4_before', hex: 'rgb(176, 198, 231)', group: 'css' },
          { class: '-', color: 'cssObozFont_risk_1_green_after', hex: 'rgb(0, 0, 0)', group: 'css' },
          { class: '-', color: 'cssObozFont_risk-2-blue_before', hex: 'rgb(142, 206, 78)', group: 'css' },
          { class: '-', color: 'cssObozFont_risk-2-blue_after', hex: 'rgb(0, 0, 0)', group: 'css' },
          { class: '-', color: 'cssObozFont_risk-3-yellow_after', hex: 'rgb(0, 0, 0)', group: 'css' },
          { class: '-', color: 'cssObozFont_risk-4-orange_after', hex: 'rgb(0, 0, 0)', group: 'css' },
          { class: '-', color: 'cssObozFont_risk-5-red_after', hex: 'rgb(255, 255, 255)', group: 'css' },
          { class: '-', color: 'cssObozFont_risk-6-black_after', hex: 'rgb(255, 255, 255)', group: 'css' },
          { class: '-', color: 'cssObozFont_switch_on_before', hex: 'rgb(200, 201, 203)', group: 'css' },
          { class: '-', color: 'cssObozFont_zagr_before', hex: 'rgb(176, 198, 231)', group: 'css' },
          { class: '-', color: 'cssObozFont_zagr_after', hex: 'rgb(255, 255, 255)', group: 'css' },
          { class: '-', color: 'cssObozFont_zagr_path3_before', hex: 'rgb(255, 255, 255)', group: 'css' },
          { class: '-', color: 'cssObozFont_zagr_path4_before', hex: 'rgb(176, 198, 231)', group: 'css' },
          { class: '-', color: 'cssObozFont_switch_off_before', hex: 'rgb(200, 201, 203)', group: 'css' },
          { class: '-', color: 'obAddressInput-results_loading_block_background-color', hex: 'rgba(255, 255, 255, 0.6)', group: 'css' },
          { class: '-', color: 'obAddressModal-obModal_leaflet_first_child_background_before', hex: '#E6E6E6', group: 'css' },
          { class: '-', color: 'obAddressModal-obModal_leaflet_a_box_shadow', hex: 'rgba(0, 0, 0, 0.3)', group: 'css' },
          { class: '-', color: 'obAddressModal-obModal_controls_box_shadow', hex: 'rgba(0, 0, 0, 0.25)', group: 'css' },
          { class: '-', color: 'obAutocomplete-border_color_blue', hex: '#D6E2F6', group: 'css' },
          { class: '-', color: 'obAutocomplete-results_loading_block_background', hex: 'rgba(255, 255, 255, 0.6)', group: 'css' },
          { class: '-', color: 'obButton-primary_color', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obButton-primary_color_disabled', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obButton-primary_color_dark_disabled', hex: '#384554', group: 'css' },
          { class: '-', color: 'obButton-primary-success', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obButton-primary-success_color_disabled', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obButton-primary-success_color_dark_disabled', hex: '#384554', group: 'css' },
          { class: '-', color: 'obCardPath-label_header_font_color', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obControlGroup-default_item_active_box_shadow', hex: 'rgba(0, 0, 0, 0.4)', group: 'css' },
          { class: '-', color: 'obControlGroup-light_item_active_box_shadow', hex: 'rgba(0, 0, 0, 0.2)', group: 'css' },
          { class: '-', color: 'obForm-title_color', hex: '#000000', group: 'css' },
          { class: '-', color: 'obForm-loading_block_background-color', hex: 'rgba(255, 255, 255, 0.6)', group: 'css' },
          { class: '-', color: 'obFuncPanel-filter_buttons_panel_item_box-shadow', hex: '#D8D8D8', group: 'css' },
          { class: '-', color: 'obFuncPanel-control_input_border', hex: '#dcdfe6', group: 'css' },
          { class: '-', color: 'obFuncPanel-control_input_color', hex: '#606266', group: 'css' },
          { class: '-', color: 'obInput-border_color_warning', hex: '#ffc31e', group: 'css' },
          { class: '-', color: 'obInput-border_blue', hex: '#D6E2F6', group: 'css' },
          { class: '-', color: 'obMainMenu-tasks_active_background', hex: 'rgba(255, 255, 255, 0.2)', group: 'css' },
          { class: '-', color: 'obMainMenu-middle_right', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obMainMenu-middle_right_bill_info_is_active', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obMainMenu-middle-button_active-border', hex: 'rgba(255, 255, 255, 0.8)', group: 'css' },
          { class: '-', color: 'obMainMenu-lang_item_color', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_i', hex: '#FFFFFF', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_tooltip_color', hex: '#333333', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_tooltip_background', hex: '#FFF1C5', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_tooltip_border', hex: '#BFBFBF', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_dropdown_tooltip_color', hex: '#000000', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_dropdown_tooltip_background', hex: '#FFF1C5', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_dropdown_tooltip_border', hex: '#BFBFBF', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_dropdown_others_color', hex: '#7b9acd', group: 'css' },
          { class: '-', color: 'obMainMenu-top_profile_dropdown_invitation_background', hex: '#00CCA6', group: 'css' }, // $deep-blue
          { class: '-', color: 'obMainMenu-top_profile_dropdown_company_background', hex: '#00CCA6', group: 'css' }, // $deep-blue
          { class: '-', color: 'obModal-body_color', hex: '#000000', group: 'css' },
          { class: '-', color: 'obModal-dialog_box_shadow', hex: 'rgba(0, 0, 0, .3)', group: 'css' },
          { class: '-', color: 'obModal-dialog_body_color', hex: '#606266', group: 'css' },
          { class: '-', color: 'obModal-dialog_headerbtn_color', hex: '#909399', group: 'css' },
          { class: '-', color: 'obModal-dialog_headerbtn_color_focus', hex: '#409EFF', group: 'css' },
          { class: '-', color: 'obModal-dialog_title_color', hex: '#303133', group: 'css' },
          { class: '-', color: 'obOrderContractorCard-border_color', hex: '#D6E2F6', group: 'css' },
          { class: '-', color: 'obOrderContractorCard-font_color_reg_number', hex: '#000000', group: 'css' },
          { class: '-', color: 'obOrderTopPanel-label_text_color', hex: '#000000', group: 'css' },
          { class: '-', color: 'obPagination-page_item_background-color_hover', hex: '#fcf8fc', group: 'css' },
          { class: '-', color: 'obPagination-page_input_border', hex: '#dcdfe6', group: 'css' },
          { class: '-', color: 'obQuestionnaire-section_toggler_border', hex: '#e2ecf7', group: 'css' },
          { class: '-', color: 'obQuestionnaire-question_title_color', hex: '#000000', group: 'css' },
          { class: '-', color: 'obQuestionnaire-preview-alert_background-color', hex: '#D6E2F6', group: 'css' },
          { class: '-', color: 'obRangeSelect-options_background_color_hover', hex: '#F5F7FA', group: 'css' },
          { class: '-', color: 'obRangeSelect-options_box_shadow', hex: 'rgba(0,0,0,.1)', group: 'css' },
          { class: '-', color: 'obSelect-loader_rounded_border', hex: '#999', group: 'css' },
          { class: '-', color: 'obSelect-list_item_subtitle_color', hex: 'rgb(158, 160, 171)', group: 'css' },
          { class: '-', color: 'obSwitcher-is_checkbox_switch_box_shadow', hex: '#cfd2e1', group: 'css' },
          { class: '-', color: 'obSwitcher-switch_box_shadow', hex: 'rgba(1, 76, 201, 0.48)', group: 'css' },
          { class: '-', color: 'obSwitcher-switch_checked_box_shadow', hex: 'rgba(1, 76, 201, 0.48)', group: 'css' },
          { class: '-', color: 'obSwitcher-switch_button_background', hex: '#cfd3e1', group: 'css' },
          { class: '-', color: 'obSwitcher-switcher_dark_label_color', hex: '#fff', group: 'css' },
          { class: '-', color: 'obSwitcherCheckbox-switch_box_shadow', hex: '#CFD2E1', group: 'css' },
          { class: '-', color: 'obSwitcherCheckbox-background_dark_switch_box', hex: '#000000', group: 'css' },
          { class: '-', color: 'obSwitcherCheckbox-background_dark_switch_button', hex: '#828D99', group: 'css' },
          { class: '-', color: 'obSwitcherCheckbox-box_shadow_dark_switch_button', hex: 'rgba(0, 0, 0, 0.25)', group: 'css' },
          { class: '-', color: 'obSwitcherCheckbox-background_switch_button', hex: '#CFD3E1', group: 'css' },
          { class: '-', color: 'obSwitcherRadio-switch_box_shadow', hex: 'rgba(1, 76, 201, 0.48)', group: 'css' },
          { class: '-', color: 'obShowMore_toggle_content_gradient_color_1_after', hex: 'rgba(255, 255, 255, 1)', group: 'css' },
          { class: '-', color: 'obShowMore_toggle_content_gradient_color_2_after', hex: 'rgba(255, 255, 255, 0)', group: 'css' },
          { class: '-', color: 'obTable-inner_loader_row_background', hex: 'rgba(255, 255, 255, 0.7)', group: 'css' },
          { class: '-', color: 'obTable-footer_gradient_color_1_before', hex: 'rgba(196, 213, 237, 0.5)', group: 'css' }, // $cssTableStyle_footer_gradient_color_1_before
          { class: '-', color: 'obTable-footer_gradient_color_2_before', hex: 'rgba(207, 222, 244, 0)', group: 'css' }, // $cssTableStyle_footer_gradient_color_2_before
          { class: '-', color: 'obTable-mark_color', hex: '#fff', group: 'css' },
          { class: '-', color: 'obTable-mark_background', hex: '#03a9f4', group: 'css' }, // $cssTableStyle_table_panel_inner_mark_background
          { class: '-', color: 'obTable-sticky_column_last_gradient_color_1', hex: '#f9fafd', group: 'css' }, // $cssTableStyle_gradient_color_1
          { class: '-', color: 'obTable-sticky_column_last_gradient_color_2', hex: '#d9e4f7', group: 'css' }, // $cssTableStyle_gradient_color_2
          { class: '-', color: 'obTable-showMore_gradient_start', hex: 'rgba(255, 255, 255, 0)', group: 'css' },
          { class: '-', color: 'obTable-showMore_gradient_end', hex: 'rgba(255, 255, 255, 1)', group: 'css' },
          { class: '-', color: 'obTable-showMore_ms_gradient_start ', hex: '#ffffff', group: 'css' },
          { class: '-', color: 'obTable-showMore_ms_gradient_end', hex: '#ffffff', group: 'css' },
          { class: '-', color: 'obTable-obHeaderTable_th_background_hover', hex: '#f2f2f2', group: 'css' }, // $cssTableStyle_th_background_hover
          { class: '-', color: 'obTable-obHeaderTable_table_empty_color', hex: '#9e9e9e', group: 'css' }, // $cssTableStyle_table-empty_color
          { class: '-', color: 'obTable-obHeaderTable_sort_direction_color', hex: '#000', group: 'css' },
          { class: '-', color: 'obTooltip-popoverArrow_border_top_color', hex: '#fff', group: 'css' },
          { class: '-', color: 'obUploader-body_border_color', hex: '#C0C0C0', group: 'css' },
          { class: '-', color: 'topMenuText', hex: '#474B5B', group: 'css' },
          { class: '-', color: 'topMenuTextHover', hex: '#191E32', group: 'css' },
          // colorsGruzi
          { class: 'green-500', color: 'green-500', hex: '#00F3C6', group: 'newGruzi' },
          { class: 'green-600', color: 'green-600', hex: '#00DDB4', group: 'newGruzi' },
          { class: 'green-700', color: 'green-700', hex: '#00CCA6', group: 'newGruzi' },
          { class: 'green-800', color: 'green-800', hex: '#00745E', group: 'newGruzi' },
          { class: 'green-900', color: 'green-900', hex: '#E8E9EB', group: 'newGruzi' },
          { class: 'gray-200', color: 'gray-200', hex: '#00B290', group: 'newGruzi' },
          { class: 'blue-300', color: 'blue-300', hex: '#2FBBFF', group: 'newGruzi' },
          { class: 'blue-700', color: 'blue-700', hex: '#004BC8', group: 'newGruzi' },
          { class: 'blue-800', color: 'blue-800', hex: '#005D8A', group: 'newGruzi' }
        ]
      }
    },
    computed: {
      obTitle () {
        return {
          margin: '10px 0'
        }
      },
      obColor () {
        return {
          display: 'inline-block',
          'font-size': '40px'
        }
      },
      filteredColors () {
        return this.colors.filter(item => {
          const search = this.searchColor.toLowerCase()
          return item.hex.toLowerCase().includes(search) || item.color.toLowerCase().includes(search)
        })
      },
      colorsCommon () {
        return this.filteredColors.filter(item => item.group === 'common')
      },
      colorsAliases () {
        return this.filteredColors.filter(item => item.group === 'aliases')
      },
      colorsCss () {
        return this.filteredColors.filter(item => item.group === 'css')
      },
      colorsGruzi () {
        return this.filteredColors.filter(item => item.group === 'newGruzi')
      }
    },
    template: `
      <div>
        <input type="text" v-model="searchColor" placeholder="name or #hex" style="border-radius: 3px; height: 30px; padding: 0 10px;" />
        <hr :style="obTitle" />
        <h4>{{ label }}</h4>
        <div class="line-color" v-for="item in colorsCommon" style="display: flex; margin-bottom: 3px;">
          <span :style="obColor" :class="'-' + item.class">■</span>
          <span class="label-color"><b>$\{{ item.color }}</b> &mdash; [ {{ item.hex }} ]</span>
        </div>
        <hr :style="obTitle" />
        <h4>Aliases <em>[aliases: color]</em></h4>
        <div class="line-color" v-for="item in colorsAliases" style="display: flex; margin-bottom: 3px;">
          <span :style="obColor" :class="'-' + item.class">■</span>
          <span class="label-color"><b>$\{{ item.class }}</b>: $\{{ item.color }} &mdash; [ {{ item.hex }} ]</span>
        </div>
        <hr :style="obTitle" />
        <h4>CSS variables</h4>
        <div class="line-color" v-for="item in colorsCss" style="display: flex; margin-bottom: 3px;">
          <span :style="[obColor, {color: item.hex}]" :class="'-' + item.class">■</span>
          <span class="label-color">$\{{ item.color }} &mdash; [ {{ item.hex }} ]</span>
        </div>
        <hr :style="obTitle" />
        <h4>New colors for GRUZI.ru</h4>
        <div class="line-color" v-for="item in colorsGruzi" style="display: flex; margin-bottom: 3px;">
          <span :style="obColor" :class="'-' + item.class">■</span>
          <span class="label-color"><b>$\{{ item.color }}</b> &mdash; [ {{ item.hex }} ]</span>
        </div>
      </div>`,
    methods: {
      action: action('clicked')
    }
  }))
