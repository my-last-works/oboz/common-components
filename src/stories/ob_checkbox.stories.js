/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObCheckbox from '../obComponents/obCheckbox'

const stories = storiesOf('ObCheckbox', module)

stories.addDecorator(withInfo)
stories.addDecorator(withKnobs)
  .add('sample', () => ({
    props: {
      label: {
        default: text('label', 'Запущено')
      },
      type: {
        default: select('type', [ 'particular', 'square', 'default' ], 'default')
      },
      disabled: {
        default: boolean('disabled', false)
      },
      required: {
        default: boolean('required', false)
      }
    },
    data () {
      return {
        model: true
      }
    },
    components: { ObCheckbox },
    template: `
    <div class="element_container">
      <ObCheckbox
        :label="label"
        :disabled="disabled"
        :type="type"
        :required="required"
        v-model="model"
      />

      <br>
      value: {{ model }}
    </div>`,
    methods: { action: action('clicked') }
  }),
  {
    info: {
      components: { ObCheckbox },
      summary: `
      \`\`\`html
        <ObCheckbox
            :label="label"
            :disabled="disabled"
            :type="type"
            :required="required"
            v-model="model"
        />
      \`\`\`
      `,
      source: false
    }
  })
