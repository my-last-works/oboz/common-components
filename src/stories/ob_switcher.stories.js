/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, boolean, select } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObSwitcher from '../obComponents/obSwitcher'

const stories = storiesOf('ObSwitcher', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    isDisabled: {
      default: boolean('isDisabled', false)
    },
    isDark: {
      default: boolean('isDark', false)
    },
    valueLeft: {
      default: select('valueLeft', [ null, 'valueLabelLeft' ], null)
    },
    valueRight: {
      default: select('valueRight', [ null, 'valueLabelRight' ], null)
    },
    labelLeft: {
      default: select('labelLeft', [ null, 'Label' ], 'Label')
    },
    labelRight: {
      default: select('labelRight', [ null, 'Label2' ], 'Label2')
    }
  },
  components: {
    ObSwitcher
  },
  data () {
    return {
      value: true
    }
  },
  template: `
  <div class="element_container">
    <div :style="{ background: isDark ? '#232C36' : '#fff', padding: '10px', color: isDark ? '#fff' : '#000' }">
      <ObSwitcher
        v-model="value"
        :is-disabled="isDisabled"
        :is-dark="isDark"
        :label-right="labelRight"
        :label-left="labelLeft"
        :value-left="valueLeft"
        :value-right="valueRight"
      />
      <br>
      <br>
      <br>
      <strong>Output: </strong>{{ value }}
    </div>
  </div>`,
  methods: {
    action: action('clicked')
  }
}),
{
  info: {
    components: { ObSwitcher },
    summary: `
      \`\`\`html
        <ObSwitcher
          v-model="value"
          :is-disabled="isDisabled"
          :is-dark="isDark"
          :label-right="labelRight"
          :label-left="labelLeft"
          :value-left="valueLeft"
          :value-right="valueRight"
        />
      \`\`\`
    `,
    source: false
  }
})
