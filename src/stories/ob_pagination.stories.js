/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, number, boolean, select } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObPagination from '../obComponents/obPagination'

const stories = storiesOf('ObPagination', module)
stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    page: {
      default: number('page', 1)
    },
    count: {
      default: number('count', 500)
    },
    pageLimit: {
      default: select('pageLimit', [ 10, 20, 50 ], 10)
    },
    isPageInput: {
      type: Boolean,
      default: boolean('isPageInput', true)
    },
    isWide: {
      default: boolean('isWide', false)
    }
  },
  components: { ObPagination },
  template: `
  <div class="element_container">
      <ObPagination
        :page="page"
        :count="count"
        :page-limit="pageLimit"
        :is-page-input="isPageInput"
        :isWide="isWide"
      />
  </div>`,
  methods: {
    action: action('clicked')
  }
}),
{
  info: {
    components: { ObPagination },
    summary: `
      \`\`\`html
        <ObPagination
          :page="page"
          :count="count"
          :page-limit="pageLimit"
          :is-page-input="isPageInput"
        />
      \`\`\`
      `,
    source: false
  }
})
