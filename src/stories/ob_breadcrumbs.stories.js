/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { object, withKnobs } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObBreadcrumbs from '../obComponents/obBreadcrumbs/index'

const stories = storiesOf('ObBreadcrumbs', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    dataArray: {
      type: Array,
      default: object('dataArray', [
        {
          id: 1,
          title: 'Маршрут и груз',
          isSelected: true
        },
        {
          id: 5,
          title: 'Логцепочки',
          isSelected: false
        },
        {
          id: 2,
          title: 'Бриф',
          isSelected: false
        },
        {
          id: 3,
          title: 'Параметры',
          isSelected: false
        },
        {
          id: 4,
          title: 'Услуги',
          isSelected: false
        }
      ])
    }
  },
  data () {
    return {
      value: ''
    }
  },
  components: { ObBreadcrumbs },
  template: `   
    <div class="element_container">
      <ObBreadcrumbs :data="dataArray" :width="width" />
    </div>
  `
}),
{
  info: {
    components: { ObBreadcrumbs },
    summary: `
      \`\`\`html
        <ObBreadcrumbs :data="dataArray" :width="width" />
      \`\`\`
      `,
    source: false
  }
})
