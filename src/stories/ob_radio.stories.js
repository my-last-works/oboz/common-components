/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import { action } from '@storybook/addon-actions'
import ObRadio from '../obComponents/obRadio/index.vue'

const stories = storiesOf('ObRadio', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    label1: {
      default: text('label1', 'Нефть (String)')
    },
    label2: {
      default: text('label2', '1 (Number)')
    },
    label3: {
      default: text('label3', 'Неправда (Boolean)')
    },
    tooltip: {
      default: text('tooltip', 'Напишите свой tooltip')
    },
    disabled: {
      default: boolean('disabled', false)
    },
    readonly: {
      default: boolean('readonly', false)
    },
    hideRadioControl: {
      default: boolean('hideRadioControl', false)
    }
  },
  computed: {
    typeof () {
      return window.typeof
    }
  },
  data () {
    return {
      value1: 'Нефть',
      value2: 1,
      value3: false,
      value4: 'html',
      label4: '<span style=\'border: 1px solid red;\'>html inside</span>',
      model: ''
    }
  },
  components: { ObRadio },
  template: `
    <div class="element_container">
      <ObRadio
        :label="label1"
        :valueInput="value1"
        :disabled="disabled"
        :tooltip="tooltip"
        :readonly="readonly"
        :hideRadioControl="hideRadioControl"
        v-model="model"
      />
        
      <ObRadio
        :label="label2"
        :valueInput="value2"
        :disabled="disabled"
        :readonly="readonly"
        :hideRadioControl="hideRadioControl"
        v-model="model"
      />

      <ObRadio
        :label="label3"
        :valueInput="value3"
        :disabled="disabled"
        :readonly="readonly"
        :hideRadioControl="hideRadioControl"
        v-model="model"
      />

      <ObRadio
        :label="label4"
        :valueInput="value4"
        :disabled="disabled"
        :readonly="readonly"
        :hideRadioControl="hideRadioControl"
        v-model="model"
      />

      <br>
      value: {{ model + " _" + typeof model }}
    </div>`,
  methods: { action: action('clicked') }
}),
{
  info: {
    components: { ObRadio },
    summary: `
      \`\`\`html
        <ObRadio
          :label="label4"
          :valueInput="value4"
          :disabled="disabled"
          v-model="model"
        />
      \`\`\`
      `,
    source: false
  }
})
