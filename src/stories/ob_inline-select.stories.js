/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { boolean, object, text, withKnobs } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObInlineSelect from '../obComponents/obInlineSelect'

storiesOf('ObInlineSelect', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    props: {
      readonly: {
        default: boolean('readonly', false)
      },
      disabled: {
        default: boolean('disabled', false)
      },
      isRequired: {
        default: boolean('isRequired', false)
      },
      inheritWidth: {
        default: boolean('inheritWidth', true)
      },
      isValid: {
        default: boolean('isValid', true)
      },
      errorTooltip: {
        default: text('errorTooltip', 'Ошибка')
      },
      maxWidth: {
        default: text('maxWidth', '100')
      },
      popperClass: {
        default: text('popperClass', 'popper-class')
      },
      label: {
        default: text('label', 'Действия')
      },
      items: {
        type: Array,
        default: object('items', [
          {
            uuid: '1',
            title: 'Любой'
          },
          {
            uuid: '2',
            title: 'Другой'
          },
          {
            uuid: '3',
            title: 'Никакой'
          },
          {
            uuid: '4',
            title: 'Какой хочешь'
          }
        ])
      }
    },
    data () {
      return {
        val: ''
      }
    },
    components: { ObInlineSelect },
    template: `
      <div class="element_container">
        Исполнитель: 
          <ObInlineSelect 
            :label="label" 
            :items="items"
            :readonly="readonly"
            :disabled="disabled"
            :isRequired="isRequired"
            :inheritWidth="inheritWidth"
            :isValid="isValid"
            :errorTooltip="errorTooltip"
            :maxWidth="maxWidth"
            :popperClass="popperClass"
            v-model="val"
          />
        <br>
        {{ val }}
      </div>`,
    methods: { action: action('clicked') }
  }),
  {
    info: {
      components: { ObInlineSelect },
      summary: `
            \`\`\`html
              <ObInlineSelect 
                :label="label" 
                :items="items" 
                v-model="val"
              />
            \`\`\`
          `,
      source: false
    }
  })
