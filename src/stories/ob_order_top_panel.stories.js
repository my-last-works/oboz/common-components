import { storiesOf } from '@storybook/vue'
import {
  withKnobs,
  object
} from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObOrderTopPanel from '../obComponents/obOrderTopPanel'
import '../helpers/VueFilters'

storiesOf('ObOrderTopPanel', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    components: { ObOrderTopPanel },
    props: {
      value: {
        type: Object,
        default: object('value', {
          number: 'PO19 555 1201',
          date: '2020-01-27T10:52:18.104Z',
          price: 73700,
          vat: 14740,
          status: {
            label: 'Контрактован',
            text: 'Заказ принят в исполнение',
            icon: { name: 'checked', color: '#10CF96' }
          }
        })
      }
    },
    data () {
      return {
        currentTab: ''
      }
    },
    template: `
      <div>

      <ObOrderTopPanel
        :value="value"
        @on-change-tab="e => { currentTab = e }"
      />
      <div style="padding: 40px">
      Текущая вкладка:
        {{ currentTab}}
      </div>
            
      </div>`
  }),
  {
    info: {
      components: { ObOrderTopPanel },
      summary: `
      \`\`\`html
        <ObOrderTopPanel
            :value="value"
            @on-change-tab="e => { currentTab = e }"
        />
      \`\`\`
      `,
      source: false
    }
  })
