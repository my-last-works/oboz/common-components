/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, object, boolean, text, number } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObQuestionnaire from '@/obComponents/obQuestionnaire'

const stories = storiesOf('ObQuestionnaire', module)

stories.addDecorator(withKnobs({
  escapeHTML: false
}))
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  components: { ObQuestionnaire },
  props: {
    fieldsDisabled: {
      default: boolean('fieldsDisabled', false)
    },
    showFuncPanel: {
      default: boolean('showFuncPanel', true)
    },
    showAddButton: {
      default: boolean('showAddButton', true)
    },
    fillMode: {
      default: boolean('fillMode', false)
    },
    countersDisabled: {
      default: boolean('countersDisabled', false)
    },
    isAddUserQuestions: {
      default: boolean('isAddUserQuestions', true)
    },
    isNoneContent: {
      default: boolean('isNoneContent', false)
    },
    passingScore: {
      default: number('passingScore', undefined)
    },
    noneContent: {
      default: text('noneContent', '<center>Пустая анкета</center>')
    },
    rightColumnBottom: {
      default: text('rightColumnBottom', '<h3 class="center-xs">Этот текст будет отображаться в правой колонке после статистики</h3>')
    },
    groups: {
      default: object('groups', [
        {
          uuid: 'c8ed8d94-549b-4ad6-8376-b40b0f5d1cca',
          title: 'Качество',
          pos: 5,
          questions: [
            {
              uuid: '2051d519-1d5b-4a2b-b8d6-c155de8648a8',
              title: 'Укажите регионы присутствия бизнеса',
              pos: 3,
              isEvaluated: true,
              type: 'MULTISELECT',
              fields: [
                {
                  uuid: 'a728aaef-0cd4-434d-a8ab-f5cef92854ee',
                  title: 'ЦФО',
                  type: 'BOOLEAN',
                  weight: '1',
                  value: false
                },
                {
                  uuid: '525de7bc-5060-4c8b-aadf-99867fcc4702',
                  title: 'СЗФО',
                  type: 'BOOLEAN',
                  weight: '2',
                  value: false
                },
                {
                  uuid: '42abf075-b854-4624-9e06-cd17014e042c',
                  title: 'ЮФО',
                  type: 'BOOLEAN',
                  weight: '3',
                  value: true
                },
                {
                  uuid: '9374bd01-eaff-43f9-82bc-42c9a0230137',
                  title: 'СКФО',
                  type: 'BOOLEAN',
                  weight: '4',
                  value: false
                },
                {
                  uuid: '8a1c2188-e03e-43d7-8725-582d3c12fcc7',
                  title: 'ПФО',
                  type: 'BOOLEAN',
                  weight: '5',
                  value: true
                },
                {
                  uuid: '17b5fb3f-f6dc-41e2-b320-879e5f63d29c',
                  title: 'УФО',
                  type: 'BOOLEAN',
                  weight: '6',
                  value: false
                },
                {
                  uuid: '1f8a96a7-3ff9-444b-ae22-2ef3a60b2c06',
                  title: 'СФО',
                  type: 'BOOLEAN',
                  weight: '7'
                },
                {
                  uuid: 'fb94ce51-3b2e-46d2-96d4-e5f96969423f',
                  title: 'ДФО',
                  type: 'BOOLEAN',
                  weight: '8'
                }
              ]
            },
            {
              uuid: '1f225116-795e-419d-84c0-e5c7b75b3892',
              title: 'Главный водитель',
              pos: 6,
              type: 'TEXT',
              isEvaluated: false,
              fields: [
                {
                  uuid: '027cb472-c9ed-4d3e-9f50-15db54c66622',
                  title: 'ФИО',
                  pos: 7,
                  type: 'TEXT'
                },
                {
                  uuid: '227cb472-c9ed-4d3e-9f50-15db54c66622',
                  title: 'Должность',
                  pos: 1,
                  type: 'TEXT'
                }
              ]
            },
            {
              uuid: '6a9d4f9e-9f0d-4bbc-84d7-fb22713a6692',
              title: 'Гарантирует ли Ваша компания соблюдение стандартов и политики безопасности труда.',
              pos: 2,
              isEvaluated: true,
              type: 'SELECT',
              fields: [
                {
                  uuid: 'g7gd8d94-549b-4ad6-8376-b40b0f5d1cca',
                  title: 'Да',
                  type: 'BOOLEAN',
                  weight: 2,
                  pos: 2,
                  value: true
                },
                {
                  uuid: 'i9gd8d94-549b-4ad6-8376-b40b0f5d1ddk',
                  title: 'Нет',
                  type: 'BOOLEAN',
                  weight: 4,
                  pos: 1
                }
              ]
            },
            {
              uuid: '6a9d4f9e-9f0d-4bbc-84d7-fb22713a6692',
              title: 'Радио кнопки без свойства "weight"',
              pos: 1,
              isEvaluated: true,
              type: 'SELECT',
              fields: [
                {
                  uuid: 'g7gd8d94-549b-4ad6-8376-b40b0f5d1cca',
                  title: 'Да',
                  type: 'BOOLEAN'
                },
                {
                  uuid: 'i9gd8d94-549b-4ad6-8376-b40b0f5d1ddk',
                  title: 'Нет',
                  type: 'BOOLEAN'
                }
              ]
            }
          ]
        },
        {
          uuid: '2f38c13f-ea17-41d7-8e7b-8f8f47ea12c0',
          title: 'Контакты',
          pos: 3,
          questions: [
            {
              uuid: '7f225116-795e-419d-84c0-e5c7b75b3892',
              title: 'Ответственный бухгалтер',
              pos: 3,
              type: 'TEXT',
              isEvaluated: false,
              fields: [
                {
                  uuid: '027cb472-c9ed-4d3e-9f50-15db54c66622',
                  title: 'ФИО',
                  pos: 5,
                  type: 'TEXT',
                  value: 'Иванов Иван Иванович',
                  weight: null
                },
                {
                  uuid: '227cb472-c9ed-4d3e-9f50-15db54c66622',
                  title: 'Должность',
                  pos: 3,
                  type: 'TEXT',
                  value: 'Бухгалтер',
                  weight: null
                }
              ]
            }
          ]
        },
        {
          uuid: 'a8ed8d94-549b-4ad6-8376-b40b0f5d1cca',
          title: 'Система',
          pos: 5,
          questions: [
            {
              uuid: '6a9d4f9e-9f0d-4bbc-84d7-fb22713a6692',
              title: 'Гарантирует ли Ваша компания соблюдение стандартов и политики безопасности труда.',
              pos: 2,
              isEvaluated: true,
              type: 'SELECT',
              fields: [
                {
                  uuid: 'g7gd8d94-549b-4ad6-8376-b40b0f5d1cca',
                  title: 'Да',
                  type: 'BOOLEAN',
                  weight: 2,
                  pos: 2
                },
                {
                  uuid: 'i9gd8d94-549b-4ad6-8376-b40b0f5d1ddk',
                  title: 'Нет',
                  type: 'BOOLEAN',
                  weight: 4,
                  pos: 1
                }
              ]
            }
          ]
        }
        // {
        //   uuid: '34ed8d94-549b-4ad6-8376-b40b0f5d1cca',
        //   title: 'Дополнительные вопросы',
        //   pos: 6,
        //   isUserQuestionsAvailable: true,
        //   questions: [
        //     {
        //       fields: [],
        //       isEvaluated: false,
        //       pos: 1,
        //       title: '123467',
        //       type: 'TEXT',
        //       uuid: '0e674b12-fc00-43d3-9f05-df027dd6e298'
        //     }
        //   ]
        // }
      ])
    }
  },
  template: `
    <div style="background: #fff">
      <ObQuestionnaire
        :groups="groups"
        :show-func-panel="showFuncPanel"
        :show-add-button="showAddButton"
        :fill-mode="fillMode"
        :fields-disabled="fieldsDisabled"
        :counters-disabled="countersDisabled"
        :is-none-content="isNoneContent"
        :is-add-user-questions="isAddUserQuestions"
        :passing-score="passingScore"
      >
        <template slot="noneContent">
          <span v-html="noneContent"></span>
        </template>
        
        <template slot="rightColumnBottom">
          <span v-html="rightColumnBottom"></span>
        </template>
        
        <template slot="extraFuncToPanel">
          <i class="icon excel" />
        </template>
      </ObQuestionnaire>
    </div>`
}),
{
  info: {
    components: { ObQuestionnaire },
    summary: `
      \`\`\`html
        <ObQuestionnaire
        :groups="groups"
        :show-func-panel="showFuncPanel"
        :show-add-button="showAddButton"
        :fill-mode="fillMode"
        :fields-disabled="fieldsDisabled"
        :counters-disabled="countersDisabled"
        :is-none-content="isNoneContent"
        :is-add-user-questions="isAddUserQuestions"
        :passing-score="passingScore"
      >
        <template slot="noneContent">
          <span v-html="noneContent"></span>
        </template>
        
        <template slot="rightColumnBottom">
          <span v-html="rightColumnBottom"></span>
        </template>
        
        <template slot="extraFuncToPanel">
          <i class="icon excel" />
        </template>
      </ObQuestionnaire>
      \`\`\`
      `,
    source: false
  }
})
