import { storiesOf } from '@storybook/vue'
import { withKnobs, text, array, boolean, select } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'

import ObAutocomplete from '../obComponents/obAutocomplete'
import ObModal from '../obComponents/obModal'
import ObButton from '../obComponents/obButton'

const res = [
  {
    title: 'ЗАО «ТрансКорж»'
  },
  {
    title: 'ООО «Трансастана»',
    icon: 'risk-2-blue'
  },
  {
    title: 'ООО «Транспортные линии»',
    icon: 'risk-3-yellow'
  },
  {
    title: 'ООО «Линии электропередач»',
    icon: 'risk-2-green'
  },
  {
    title: 'ООО «Эликсир 177»'
  }
]

storiesOf('ObAutocomplete', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObAutocomplete, ObModal, ObButton },
      props: {
        label: {
          type: String,
          default: 'Исполнитель'
        },
        placeholder: {
          type: String,
          default: text('placeholder', 'Поиск')
        },
        actionIcon: {
          type: String,
          default: text('action icon', 'modal-list')
        },
        items: {
          type: Array,
          default: array('items', [])
        },
        handAction: {
          type: Boolean,
          default: boolean('action emitted on dropdown click', false)
        },
        theme: {
          default: select('theme', ['default', 'blue'], 'default')
        },
        showClear: {
          default: boolean('showClear', true)
        },
        isDisabled: {
          default: boolean('isDisabled', false)
        },
        valueExp: {
          default: text('valueExp', 'title')
        },
        isSelect: {
          default: boolean('isSelect', false)
        },
        isAlwaysAction: {
          default: boolean('isAlwaysAction', false)
        }
      },
      data () {
        return {
          contractor: null,
          contractors: [],
          popupData: {},
          guardState: false
        }
      },

      computed: {
        guardTitle () {
          return `Исполнитель ${this.popupData.title}`
        }
      },

      methods: {
        openPopup () {
          this.$refs.popup.open()
        },
        closePopup () {
          this.$refs.popup.close()
        },
        async loadData () {
          return new Promise((resolve) => {
            setTimeout(() => {
              resolve(res)
            }, 2000)
          })
        },
        onHandAction (e) {
          this.popupData = e
          this.$refs.guardPopup.open()
        },
        onSubmit () {
          this.$refs.guardPopup.close()
        },
        onCancel () {
          this.contractor = null
          this.$refs.guardPopup.close()
        }
      },
      template: `
          <div class="element_container">
              <ObAutocomplete
                      v-model="contractor"
                      :label="label"
                      :placeholder="placeholder"
                      :actionIcon="actionIcon"
                      :handAction="true"
                      :items="contractors"
                      :theme="theme"
                      :show-clear="showClear"
                      :is-disabled="isDisabled"
                      :value-exp="valueExp"
                      :remote-opt="loadData"
                      :is-select="isSelect"
                      :is-always-action="isAlwaysAction"
                      @selectAction="openPopup"
                      @handAction="onHandAction"
              >
                  <template v-slot:extra>
                      <i class="icon alert"/>
                  </template>
              </ObAutocomplete>
              <ObModal
                      ref="popup"
                      :edit-form="false"
                      title="Подбор перевозчиков"
                      :is-show-close-button="true"
              >
                  Contractors table
              </ObModal>
              <ObModal
                      ref="guardPopup"
                      :edit-form="false"
                      :title="guardTitle"
                      :is-show-close-button="true"
              >
                  Вы выбрали исполнителя вручную. Аукцион, который был автоматически назначен системой на ваш
                  заказ, будет досрочно завершен.
                  <div style="display: flex; margin-top: 10px;">
                      <ObButton label="Выбрать исполнителя" type="primary" @click="onSubmit"
                                style="margin-right: 10px;"/>
                      <ObButton label="Отмена" type="flat" @click="onCancel"/>
                  </div>
              </ObModal>
          </div>
      `,
      description: {
        obAutocomplete: {
          props: {
            value: 'Данные для v-model',
            label: 'Лейбл, отображаемый в верхней части инпута',
            placeholder: 'Текст плейсхолдера',
            actionIcon: 'Название иконки, которая находится справа в инпуте. Принимает как булевое, так и строковое значение. По умолчанию false. true = иконка modal-list, или передаем строкой свою',
            items: 'Массив элементов для выбора',
            handAction: 'Булоевое значение, показывающее должен ли компонент эмитить событие при выборе элемента в выпадающем списке. Если `true`, то компонент эмитит событие `handAction',
            guard: 'Булевое значение, показывающее серьёзность ваших намерений,  Если `false`, то всё идёт как идёт. Если `true`, то происходит отмена последнего выбора. Работает как переключатель. Компонент реагирует только на его изменение, а не на начальное состояние.'
          },
          events: {
            input: 'Стандартное событие для v-model',
            search: 'Эмимтится при вводе текста в инпут. На него нужно грузить данные в массив `items`.',
            selectAction: 'Эмитится при клике на иконку справа.',
            handAction: 'Эмитится при выборе элемента из выпадающего списка.'
          },
          slots: {
            extra: 'Слот для дополнительных иконок состояния, которые появятся справа от существующей.'
          }
        }
      }
    }
  },
  {
    info: {
      components: { ObAutocomplete },
      summary: `
      \`\`\`html
        <ObAutocomplete
          v-model="contractor"
          :label="label"
          :placeholder="placeholder"
          :actionIcon="actionIcon"
          :handAction="true"
          :items="contractors"
          :theme="theme"
          :show-clear="showClear"
          :is-disabled="isDisabled"
          :value-exp="valueExp"
          :remote-opt="loadData"
          :is-select="isSelect"
          :is-always-action="isAlwaysAction"
          @selectAction="openPopup"
          @handAction="onHandAction"
        >
          <template v-slot:extra>
              <i class="icon alert"/>
          </template>
        </ObAutocomplete>
      \`\`\`
      `,
      source: false
    }
  })
