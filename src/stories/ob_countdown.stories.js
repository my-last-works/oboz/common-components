/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, number, boolean } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObCountdown from '../obComponents/obCountdown/index'

storiesOf('ObCountdown', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { ObCountdown },
      props: {
        duration: {
          default: number('duration', 1000)
        },
        fromDate: {
          default: text('fromDate', '')
        },
        toDate: {
          default: text('toDate', '')
        },
        finishedText: {
          type: String,
          default: text('finishedText', '-')
        },
        isIcon: {
          default: boolean('isIcon', false)
        },
        strIcon: {
          type: String,
          default: text('strIcon', 'clock')
        },
        isOnlyIcon: {
          default: boolean('isOnlyIcon', false)
        }
      },
      data () {
        return {
          remaining: {}
        }
      },
      methods: {
        funcIsDanger (obj) {
          this.remaining = obj
          return obj.seconds < 60
        },
        finished () {
          alert('finished')
        }
      },
      template: `
        <div class="element_container">
          <strong>Output:</strong><br><br>
          <ObCountdown
            :duration="duration"
            :from-date="fromDate"
            :to-date="toDate"
            :finished-text="finishedText"
            :is-icon="isIcon"
            :str-icon="strIcon"
            :is-only-icon="isOnlyIcon"
            :func-is-danger="funcIsDanger"
            @finished="finished"
          >
            <template slot="finished">
              Вот и всё, <span class="-persian-red">ребята</span>!
            </template>
          </ObCountdown>
          <hr style="border-bottom: 2px #ccc solid; margin: 10px 0 20px 0;">
          <div>
             При изменении параметров <strong>duration</strong>, <strong>fromDate</strong> или <strong>toDate</strong>, счетчик при необходимости перезапускается.
          </div>
          <br>
          Даты имеют тип <strong>Date</strong> или <strong>String</strong>. Для проверки можете указать в формате 2019-04-20T16:30 (если указана невалидная дата, будет взята текущая)
          <br>
          <br>
          <div>
                Если указан только <strong>duration</strong> - отображается оставшееся время до <strong>now + duration</strong>. Обновление раз в секунду<br>
                Если указан только <strong>fromDate</strong> - отображается прошедшее время от <strong>fromDate</strong>. Обновление раз в секунду<br> 
                Если указан только <strong>toDate</strong> - отображается оставшееся время до <strong>toDate</strong>. Обновление раз в секунду<br> 
                Если указаны <strong>fromDate</strong> и <strong>toDate</strong> - отображается прошедшее время между этими датами, динамика в таком случае отключается <br>. 
                
          </div>
          <div>По истечению таймера будет выброшено событие <strong>finished</strong></div>
          Если указан только <strong>fromDate</strong> - цвет не меняется в любом случае
          <br>
          <br>
          <div>
             <strong>funcIsDanger</strong>: функция для окрашивания таймера в красный, принимает объект формата
             <br>
             <pre>
{{ remaining }}       
             </pre>
             Значения указаны в абсолютных величинах, округление вниз. now - дата от которой ведется отсчет
          </div>
          
          <br>
          <br>
          <div>
             При отображении только иконки в крайсный красится иконка, при наличии времени красится только время.   
          </div>
         
        </div>`
    }
  },
  {
    info: {
      components: { ObCountdown },
      summary: `
      \`\`\`html
        <ObCountdown
            :duration="duration"
            :from-date="fromDate"
            :to-date="toDate"
            :finished-text="finishedText"
            :is-icon="isIcon"
            :str-icon="strIcon"
            :is-only-icon="isOnlyIcon"
            :func-is-danger="funcIsDanger"
            @finished="finished"
          >
            <template slot="finished">
              Вот и всё, <span class="-persian-red">ребята</span>!
            </template>
          </ObCountdown>
      \`\`\`
      `,
      source: false
    }
  })
