/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import {
  withKnobs,
  object
} from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObOrderRouteCard from '@/obComponents/obOrderRouteCard/index.vue'

storiesOf('ObOrderRouteCard', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    components: { ObOrderRouteCard },
    props: {
      test4: {
        default: object('test4', {
          pointFrom: 'Москва',
          pointTo: 'Самара',
          items: [
            { uuid: 'log-001', type: 'log', done: true, current: false, status: 'системное', message: 'Заказ новый', date: '23.08.2019', time: '23:15' },
            { uuid: 'log-002', type: 'log', done: true, current: false, status: 'системное', message: 'Начало исполнения рейса', date: '24.08.2019', time: '01:15' },
            { uuid: 'log-003',
              type: 'location',
              done: true,
              current: false,
              point: 'from',
              status: '',
              eta: { plan: { arrive: '08:49', depart: '10:49' }, fact: { arrive: '08:55', depart: '10:32' } },
              works: [
                { date: '23.08.2019', time: '09:11', type: 'УСЛ', title: 'Растентовка', description: 'У 047 Погрузка боковая', person: 'Дмитрий Андреев', contractor: 'ООО Обоз Диджитал' },
                { date: '23.08.2019', time: '09:32', type: 'УСЛ', title: 'ПРР', description: 'У 019  Выкатка / закатка рохлей водит...', person: 'Дмитрий Андреев', contractor: 'ООО Обоз Диджитал' }
              ],
              pointName: 'ООО “ФМ Лоджистик Восток”',
              pointAddress: 'Россия, Московская область, г. Долгопрудный, ул. Новое шоссе, 34',
              date: '26.08.2019',
              time: '09:00 — 11:00' },
            { uuid: 'log-005',
              type: 'location',
              done: true,
              current: false,
              point: 'to',
              status: '',
              eta: { plan: { arrive: '20:49', depart: '22:49' } },
              pointName: 'ООО “Нестле Фуд”',
              pointAddress: 'Россия, Самарская область, г. Кинель, ул. Промышленная, 13',
              date: '26.08.2019',
              time: '21:00 — 23:00' },
            { uuid: 'log-020', type: 'log', done: true, current: false, status: 'системное', message: 'Окончание исполнения рейса', date: '26.08.2019', time: '23:15' }
          ]
        })
      },
      test3: {
        default: object('test3', {
          pointFrom: 'Москва',
          pointTo: 'Самара',
          items: [
            { uuid: 'log-001', type: 'log', done: true, current: false, status: 'системное', message: 'Заказ новый', date: '23.08.2019', time: '23:15' },
            { uuid: 'log-003',
              type: 'location',
              done: false,
              current: true,
              point: 'from',
              status: '',
              eta: { plan: { arrive: '08:49', depart: '10:49' }, fact: { arrive: '08:55', depart: '10:32' } },
              works: [
                { date: '23.08.2019', time: '09:11', type: 'УСЛ', title: 'Растентовка', description: 'У 047 Погрузка боковая', person: 'Дмитрий Андреев', contractor: 'ООО Обоз Диджитал' },
                { date: '23.08.2019', time: '09:32', type: 'УСЛ', title: 'ПРР', description: 'У 019  Выкатка / закатка рохлей водит...', person: 'Дмитрий Андреев', contractor: 'ООО Обоз Диджитал' }
              ],
              pointName: 'ООО “ФМ Лоджистик Восток”',
              pointAddress: 'Россия, Московская область, г. Долгопрудный, ул. Новое шоссе, 34',
              date: '26.08.2019',
              time: '09:00 — 11:00' },
            { uuid: 'log-005',
              type: 'location',
              done: false,
              current: false,
              point: 'to',
              status: '',
              eta: { plan: { arrive: '20:49', depart: '22:49' } },
              pointName: 'ООО “Нестле Фуд”',
              pointAddress: 'Россия, Самарская область, г. Кинель, ул. Промышленная, 13',
              date: '26.08.2019',
              time: '21:00 — 23:00' }
          ]
        })
      },
      test2: {
        default: object('test2', {
          pointFrom: 'Москва',
          pointTo: 'Самара',
          items: [
            {
              uuid: 'log-001',
              type: 'log',
              done: true,
              current: false,
              status: 'системное',
              message: 'Заказ новый',
              date: '23.08.2019',
              time: '23:15'
            },
            {
              uuid: 'log-002',
              type: 'log',
              done: true,
              current: false,
              status: 'системное',
              message: 'Начало исполнения рейса',
              date: '24.08.2019',
              time: '01:15'
            },
            { uuid: 'log-006', type: 'state', done: false, current: true, message: 'ТС в пути' },
            {
              uuid: 'log-003',
              type: 'location',
              done: false,
              current: false,
              point: 'from',
              status: '',
              eta: { plan: { arrive: '08:49', depart: '10:49' } },
              pointName: 'ООО “ФМ Лоджистик Восток”',
              pointAddress: 'Россия, Московская область, г. Долгопрудный, ул. Новое шоссе, 34',
              date: '26.08.2019',
              time: '09:00 — 11:00'
            },
            {
              uuid: 'log-004',
              type: 'log',
              done: false,
              current: false,
              status: 'системное',
              message: 'Какое-то событие',
              date: '25.08.2019',
              time: '03:15'
            },
            {
              uuid: 'log-005',
              type: 'location',
              done: false,
              current: false,
              point: 'to',
              status: '',
              eta: { plan: { arrive: '20:49', depart: '22:49' } },
              pointName: 'ООО “Нестле Фуд”',
              pointAddress: 'Россия, Самарская область, г. Кинель, ул. Промышленная, 13',
              date: '26.08.2019',
              time: '21:00 — 23:00'
            }
          ]
        })
      },
      test1: {
        default: object('test1', {
          pointFrom: 'Москва',
          pointTo: 'Самара',
          items: [
            {
              uuid: 'log-001',
              type: 'log',
              done: true,
              current: true,
              status: 'системное',
              message: 'Заказ новый',
              date: '23.08.2019',
              time: '23:15'
            },
            {
              uuid: 'log-003',
              type: 'location',
              done: false,
              current: false,
              point: 'from',
              status: 'ожидание начала рейса',
              pointName: 'ООО “ФМ Лоджистик Восток”',
              pointAddress: 'Россия, Московская область, г. Долгопрудный, ул. Новое шоссе, 34',
              date: '26.08.2019',
              time: '09:00 — 11:00'
            },
            {
              uuid: 'log-005',
              type: 'location',
              done: false,
              current: false,
              point: 'to',
              status: 'ожидание начала рейса',
              pointName: 'ООО “Нестле Фуд”',
              pointAddress: 'Россия, Самарская область, г. Кинель, ул. Промышленная, 13',
              date: '26.08.2019',
              time: '21:00 — 23:00'
            }
          ]
        })
      }
    },
    template: `
      <div>
          <div>Пример №4</div>
          <ObOrderRouteCard :value="test4"/>
          <div>Пример №3</div>
          <ObOrderRouteCard :value="test3"/>
          <div>Пример №2</div>
          <ObOrderRouteCard :value="test2"/>
          <div>Пример №1</div>
          <ObOrderRouteCard :value="test1"/>
      </div>`
  }),
  {
    info: {
      components: { ObOrderRouteCard },
      summary: `
      \`\`\`html
        <ObOrderRouteCard :value="test4"/>
      \`\`\`
      `,
      source: false
    }
  })
