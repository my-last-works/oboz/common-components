/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObModal from '../obComponents/obModal'
import ObInput from '../obComponents/obInput'
import ObButton from '../obComponents/obButton'
import ObForm from '../obComponents/obForm'

const stories = storiesOf('ObModal', module)
stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    title: {
      default: text('title', 'Modal title')
    },
    titleSecond: {
      default: text('titleSecond', 'Добавление')
    },
    size: {
      default: select('size', [ 'small', 'medium', 'large', 'extra-large', 'max' ], 'large')
    },
    isCenter: {
      default: boolean('isCenter', false)
    },
    customClass: {
      default: text('customClass', '')
    },
    isReadOnly: {
      default: boolean('isReadOnly', false)
    },
    isOnlyClose: {
      default: boolean('isOnlyClose', false)
    },
    isShowBackButton: {
      default: boolean('isShowBackButton', false)
    },
    isShowCloseButton: {
      default: boolean('isShowCloseButton', false)
    },
    isAppendToBody: {
      default: boolean('isAppendToBody', true)
    },
    isNested: {
      default: boolean('isNested', false)
    },
    isShowControls: {
      default: boolean('isShowControls', true)
    },
    okText: {
      default: text('okText', 'Добавить')
    },
    cancelText: {
      default: text('cancelText', 'Отмена')
    },
    isSubmitDisabled: {
      default: select('isSubmitDisabled', [ undefined, true, false ], undefined)
    },
    isShowPagination: {
      default: boolean('isShowPagination', false)
    },
    isError: {
      default: boolean('isError', false)
    },
    isCloseOnOverlay: {
      default: boolean('isCloseOnOverlay', true)
    },
    isActiveElement: {
      default: select('isActiveElement', [ null, true, false ], null)
    },
    btnModal: {
      default: text('btnModal', '')
    },
    strConfirmModalTitle: {
      default: text('strConfirmModalTitle', 'Закрыть без сохранения?')
    },
    strConfirmModalText: {
      default: text('strConfirmModalText', 'при закрытии произойдет анигиляция данных ')
    },
    strConfirmModalOkButton: {
      default: text('strConfirmModalOkButton', 'Да')
    },
    strConfirmModalCancelButton: {
      default: text('strConfirmModalCancelButton', 'Нет')
    }
  },
  data () {
    return {
      formData: {},
      dropdownMenu: [
        {
          title: 'Удалить',
          className: 'color-error',
          href: 'https://alfilatov.com/posts/run-chrome-without-cors/'
        },
        {
          title: 'История',
          className: 'color-success',
          to: { name: 'data-grid' }
        },
        {
          title: 'Редактирование',
          method: this.onClickEdit
        }
      ],
      isActiveElementValue: this.isActiveElement
    }
  },
  components: {
    ObModal,
    ObButton,
    ObInput,
    ObForm
  },
  template: `
    <div class="element_container">
      <ObButton @click="openModal" label="Показать"/>
      <ObModal
        ref="ob-modal"
        :size="size"
        :is-center="isCenter"
        :is-show-pagination="isShowPagination"
        :title="title"
        :title-second="titleSecond"
        :custom-class="customClass"
        :is-append-to-body="isAppendToBody"
        :is-nested="isNested"
        :dropdown-menu="dropdownMenu"
        :ok-text="okText"
        :cancel-text="cancelText"
        :is-submit-disabled="isSubmitDisabled"
        :is-show-controls="isShowControls"
        :is-read-only="isReadOnly"
        :is-only-close="isOnlyClose"
        :is-error="isError"
        :is-close-on-overlay="isCloseOnOverlay"
        :is-show-back-button="isShowBackButton"
        :is-show-close-button="isShowCloseButton"
        :is-active-element="isActiveElementValue"
        :btn-modal="btnModal"
        :str-confirm-modal-title="strConfirmModalTitle"
        :str-confirm-modal-text="strConfirmModalText"
        :str-confirm-modal-ok-button="strConfirmModalOkButton"
        :str-confirm-modal-cancel-button="strConfirmModalCancelButton"
        @change-active="onChangeActive"
        @on-btn-modal="openModalChild"
      >
          <p>Внутрь контейнера можно вставлять блоки любой высоты. Высота окна подстроится автоматически. Внутрь контейнера можно вставлять блоки любой высоты. Высота окна подстроится автоматически.</p>
          <p>Внутрь контейнера можно вставлять блоки любой высоты. Высота окна подстроится автоматически. Внутрь контейнера можно вставлять блоки любой высоты. Высота окна подстроится автоматически.</p>
          <br>
          <div style="border: solid; padding: 5px">
          <br>
          Есди внутри модалки размещен ObForm, то при клике мимо модального окна, перед закрытием проверяется статус изменения данных в форме.
          <br>
          В случае наличия изменений выходит дополнительное подтвержденияе закрытия
          <br>
          <br>
          <br>
           <ObForm
            :form-data="formData"
            :is-show-controls="true"
            >
            <ObInput
                    v-model="formData.field1"
                    label="Поле ввода"
                    placeholder="Введите текст"
                    property="field1"
            />
          </ObForm>
          </div>
      </ObModal>
      <ObModal
        ref="ob-modal-child"
        size="small"
        :title="btnModal"
        :is-show-controls="true"
        :is-center="true"
      >
      </ObModal>
    </div>`,
  watch: {
    isActiveElement (val) {
      this.isActiveElementValue = val
    }
  },
  methods: {
    action: action('clicked'),
    openModal () {
      this.formData = {
        field1: '' }
      this.$refs['ob-modal'].open()
    },
    openModalChild () {
      this.$refs['ob-modal-child'].open()
    },
    onChangeActive (val) {
      this.isActiveElementValue = val
    }
  }
}),
{
  info: {
    components: { ObModal },
    summary: `
      \`\`\`html
        <ObModal
          ref="ob-modal-child"
          size="small"
          :title="btnModal"
          :is-show-controls="true"
          :is-center="true"
        >
        </ObModal>
        
        С формой:
        
        <ObModal
          ref="ob-modal"
          :size="size"
          :is-center="isCenter"
          :is-show-pagination="isShowPagination"
          :title="title"
          :title-second="titleSecond"
          :custom-class="customClass"
          :is-append-to-body="isAppendToBody"
          :is-nested="isNested"
          :dropdown-menu="dropdownMenu"
          :ok-text="okText"
          :cancel-text="cancelText"
          :is-submit-disabled="isSubmitDisabled"
          :is-show-controls="isShowControls"
          :is-read-only="isReadOnly"
          :is-only-close="isOnlyClose"
          :is-error="isError"
          :is-close-on-overlay="isCloseOnOverlay"
          :is-show-close-button="isShowCloseButton"
          :is-active-element="isActiveElementValue"
          :btn-modal="btnModal"
          :str-confirm-modal-title="strConfirmModalTitle"
          :str-confirm-modal-text="strConfirmModalText"
          :str-confirm-modal-ok-button="strConfirmModalOkButton"
          :str-confirm-modal-cancel-button="strConfirmModalCancelButton"
          @change-active="onChangeActive"
          @on-btn-modal="openModalChild"
        >
          <ObForm
            :form-data="formData"
            :is-show-controls="true"
            >
              <ObInput
                v-model="formData.field1"
                label="Поле ввода"
                placeholder="Введите текст"
                property="field1"
              />
          </ObForm>
        </ObModal>
      \`\`\`
    `,
    source: false
  }
})
