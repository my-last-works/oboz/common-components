/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import ObFuncPanel from '../obComponents/obFuncPanel'
import { text, boolean, object, withKnobs } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'

storiesOf('ObFuncPanel', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      props: {
        showColumnFilter: {
          default: boolean('showColumnFilter', true)
        },
        showSearch: {
          default: boolean('showSearch', true)
        },
        showFilter: {
          default: boolean('showFilter', true)
        },
        showGroup: {
          default: boolean('showGroup', true)
        },
        showSort: {
          default: boolean('showSort', true)
        },
        showEdit: {
          default: boolean('showEdit', true)
        },
        showRefresh: {
          default: boolean('showRefresh', true)
        },
        showAddButton: {
          default: boolean('showAddButton', true)
        },
        addButtonText: {
          default: text('addButtonText', 'Добавить')
        },
        columns: {
          type: Array,
          default: object('columns', [
            {
              title: 'Название',
              key: 'title',
              _isEnabled: true
            },
            {
              title: 'Значение',
              key: 'value',
              _isEnabled: true
            },
            {
              title: 'Дата и время',
              key: 'when',
              _isEnabled: true
            }
          ])
        },
        addButtons: {
          type: Array,
          default: object('addButtons', [
            {
              id: 'RFI',
              text: 'Создать RFI'
            },
            {
              id: 'RFQ',
              text: 'Создать RFQ'
            }
          ])
        },
        addButtonsOptions: {
          default: object('addButtonsOptions', {
            group: true,
            text: 'Создать тендер'
          })
        },
        filterButtons: {
          type: Array,
          default: object('filterButtons', [
            { title: 'Все' },
            { title: 'Актуальные', _isActive: true },
            { title: 'Неактивные' }
          ])
        }
      },
      data () {
        return {
          countsMap: new Map(),
          counts: []
        }
      },
      components: { ObFuncPanel },
      template: `
        <div style="margin-top: 100px;">

        <ob-func-panel
            :columns="columns"
            :showColumnFilter="showColumnFilter"
            :showSearch="showSearch"
            :showFilter="showFilter"
            :showGroup="showGroup"
            :showSort="showSort"
            :showEdit="showEdit"
            :showRefresh="showRefresh"
            :showAddButton="showAddButton"
            :addButtonText="addButtonText"
            @search="onSearch"
        >
          <template #buttons>
            <div style="display: flex; justify-content: center; outline: 1px dashed #9DA0AC">
              <i class="icon email"></i>
              Buttons panel
            </div>
          </template>

          <template #controls>
            <span style="display:flex; justify-content: center;flex-direction: column;">
              controls-items
            </span>
          </template>

        </ob-func-panel>

        <br/><br/>

        <ob-func-panel
            :columns="columns"
            :showColumnFilter="showColumnFilter"
            :showSearch="showSearch"
            :showFilter="showFilter"
            :showGroup="showGroup"
            :showSort="showSort"
            :showEdit="showEdit"
            :showRefresh="showRefresh"
            :add-buttons="addButtons"
            :add-buttons-options="addButtonsOptions"
            :filter-buttons="filterButtons"
            @on-change-filter-button="onChangeFilterButton"
            @search="onSearch"
            @add="onAdd"
            @sort="onSort"
            @group="onGroup"
        >
          <template slot="extraFuncToPanel">
            <button
                @click="extraButton"
            >
              <span>Дополнительный функционал</span>
            </button>
          </template>
        </ob-func-panel>

        <div
            v-for="(it, index) in counts"
            :key="it[0]"
        >
          {{ it[0] }}:{{ it[1] }}
        </div>
        </div>`,
      methods: {
        onChangeFilterButton (button) {
        },
        onSearch (e) {
          console.log(e)
        },
        onAdd (it) {
          let count = this.countsMap.get(it.id)
          this.countsMap.set(it.id, count ? count + 1 : 1)
          this.counts = Array.from(this.countsMap)
        },
        onSort (e) {
        },
        onGroup (e) {
        },
        extraButton () {
        }
      },
      description: {
        ObFuncPanel: {
          props: {
            showGroup: 'Показывать иконку группировки'
          },
          events: {
            'group': 'Вызывается при клике на группировку',
            'sort': 'Вызывается при клике на сортировку'
          }
        }
      }
    }
  }, {
    info: {
      summary: `
        \`\`\`html
        <ObFuncPanel
            :columns="columns"
            :showColumnFilter="showColumnFilter"
            :showSearch="showSearch"
            :showFilter="showFilter"
            :showGroup="showGroup"
            :showSort="showSort"
            :showEdit="showEdit"
            :showRefresh="showRefresh"
            :showAddButton="showAddButton"
            :addButtonText="addButtonText"
            @search="onSearch"
            @onEdit="onEdit"
        >
        \`\`\`
      `,
      source: false
    }
  }
  )
