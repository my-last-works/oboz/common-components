/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { number, withKnobs } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObLoaderOboz from '../obComponents/obLoaderOboz/index.vue'

storiesOf('ObLoaderOboz', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    props: {
      width: {
        default: number('width', 40)
      },
      height: {
        default: number('height', 40)
      },
      duration: {
        default: number('duration', 4)
      }
    },
    components: { ObLoaderOboz },
    template: `
      <div class="element_container">
        <ObLoaderOboz :width="width" :height="height" :duration="duration" />
      </div>
    `,
    methods: {
      clickToObject () {
      },
      onChangeLocale () {
      }
    }
  }),
  {
    info: {
      components: { ObLoaderOboz },
      summary: `
          \`\`\`html
            <ObLoaderOboz 
                :width="width" 
                :height="height" 
                :duration="duration"
            />
          \`\`\`
      `,
      source: false
    }
  })
