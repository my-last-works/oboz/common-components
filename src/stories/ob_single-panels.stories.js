import { storiesOf } from '@storybook/vue'
import { withInfo } from 'storybook-addon-vue-info'
import ObLayout from '../obComponents/obLayout'
import SinglePane from '../obComponents/obSinglePanels'

storiesOf('ObSinglePanels', module)
  .addDecorator(withInfo)
  .add('sample', () => ({
    data () {
      return {
      }
    },
    components: { SinglePane, ObLayout },
    template: `
            <ObLayout @change-locale="onChangeLocale"   :top-level-members="menu || []">
                <SinglePane capt="Названиe">
                    <template v-slot:header>
                        <span> Контролы </span>
                    </template>
                    <p> Контент </p>
                </SinglePane>
            </ObLayout>
        `,
    methods: {
      onChangeLocale: () => {}
    }
  }),
  {
    info: {
      summary: `
      \`\`\`html
        <ObLayout @change-locale="onChangeLocale"   :top-level-members="menu || []">
            <SinglePane capt="Названиe">
                <template v-slot:header>
                    <span> Контролы </span>
                </template>
                <p> Контент </p>
            </SinglePane>
        </ObLayout>
        \`,
      \`\`\`
      `,
      source: false
    }
  })
