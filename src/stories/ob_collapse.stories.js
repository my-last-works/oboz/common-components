/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import { action } from '@storybook/addon-actions'

import ObCollapse from '../obComponents/obCollapse'
import ObCollapseItem from '../obComponents/obCollapse/collapse-item'

storiesOf('ObCollapse', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('пример', () => {
    return {
      components: { ObCollapse, ObCollapseItem },
      props: {
        accordion: {
          type: Boolean,
          default: boolean('accordion', true)
        },
        value: {
          type: [Array, String, Number],
          default: text('value', [0, 1])
        }
      },
      data () {
        return {
          list: [
            {
              title: 'Заголовок 1',
              content: 'Какой-то контент 1',
              disabled: false
            },
            {
              title: 'Заголовок 2',
              content: 'Какой-то контент 2',
              disabled: false
            },
            {
              title: 'Заголовок 3',
              content: 'Какой-то контент 3',
              disabled: true
            }
          ]
        }
      },
      computed: {
        headerStyle () {
          let res = {}
          res['margin'] = `20px 0 15px`
          res['padding-top'] = `10px`
          res['border-top'] = `1px dashed #333`
          return res
        }
      },
      methods: {
        changeHandle ($event) {
          action('onChange')($event)
        }
      },
      template: `
        <div style='padding: 20px;'>
          <h2 style='margin-bottom: 20px;'>Collapse</h2>
          <h3 :style="headerStyle">Аккордеон. Только 1 элемент будет раскрыт.</h3>
          <ObCollapse :accordion="accordion" :value="0">
            <ObCollapseItem
                v-for="(item, index) in list" :key="index"
                :title="item.title">
              <div>
                {{ item.content }}
              </div>
            </ObCollapseItem>
          </ObCollapse>
          <h3 :style="headerStyle">Развернуть несколько элементов</h3>
          <ObCollapse :value="value">
            <ObCollapseItem
                v-for="(item, index) in list" :key="index"
                :title="item.title" :disabled="item.disabled">
              <div>
                {{ item.content }}
              </div>
            </ObCollapseItem>
          </ObCollapse>
          <h3 :style="headerStyle">Вложенные элементы. @onChange</h3>
          <ObCollapse @on-change="changeHandle" value="0">
            <ObCollapseItem
                v-for="(item, index) in list" :key="index"
                :title="item.title">
              <div>
                {{ index !== 0 ? item.content : '' }}
                <ObCollapse accordion v-if="index === 0">
                  <ObCollapseItem title="Название вложенного элемента">
                    <div>Вложенный элемент</div>
                  </ObCollapseItem>
                </ObCollapse>
              </div>
            </ObCollapseItem>
          </ObCollapse>
          <h3 :style="headerStyle">Кастомные заголовки.</h3>
          <ObCollapse accordion :value="value">
            <ObCollapseItem name="collapse1">
              <div slot="title">1 <i class="icon calendar-sm" /></div>
              <div>контент 1</div>
            </ObCollapseItem>
            <ObCollapseItem name="collapse2">
              <div slot="title">2 <i class="icon battery_level" /></div>
              <div>контент 2</div>
            </ObCollapseItem>
            <ObCollapseItem>
              <div slot="title">3 <i class="icon close-xs" /></div>
              <div>контент 3</div>
            </ObCollapseItem>
          </ObCollapse>
        </div>`,

      description: {
        ObCollapse: {
          props: {
            accordion: 'режим аккордеона (т.е. раскрывать не более одного элемента)',
            value: 'значение текущего активного элемента name'
          },
          events: {
            'on-change': 'Вызывается при переключении. Возвращает ключ элемента(ов) из массива элементов'
          }
        },
        ObCollapseItem: {
          props: {
            title: 'Заголовок элемента (имеет slots для кастомной настройки).',
            name: 'Название текущего элемента, соответствует полю value в Collapse. Значение index будет использоваться, если не заполнено.',
            disabled: 'Запретить открывать элемент.'
          },
          slots: {
            title: 'Заголовок элемента.'
          }
        }
      }
    }
  },
  {
    info: {
      summary: `
        \`\`\`html
        <ObCollapse accordion :value="[0]">
          <ObCollapseItem title="Параметры">Текст про параметры</ObCollapseItem>
        </ObCollapse>
        \`\`\`
      `,
      source: false
    }
  })
