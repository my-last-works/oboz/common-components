/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { object, number, text, boolean, withKnobs } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObTabs from '../obComponents/obTabs'
import ObTabsPane from '../obComponents/obTabs/ObTabsPane'

storiesOf('ObTabs', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    components: {
      ObTabsPane,
      ObTabs
    },
    data () {
      return {
        tabChangesCount: -1,
        tabChangedMsg: ''
      }
    },
    props: {
      buttonMargin: {
        default: number('buttonMargin', 10)
      },
      valueExp: {
        default: text('valueExp', 'title')
      },
      noBorder: {
        default: boolean('noBorder', false)
      },
      dataArray: {
        type: Array,
        default: object('dataArray', [
          {
            title: 'Первый таб (c тултипом)',
            isSelected: true,
            notification: 'error',
            tooltip: { content: 'Контент тултипа' },
            content: 'Ребята, не стоит вскрывать эту тему. Вы молодые, шутливые, вам все легко. Это не то. Это не Чикатило и даже не архивы спецслужб. Сюда лучше не лезть. Серьезно, любой из вас будет жалеть. Лучше закройте тему и забудьте, что тут писалось. Я вполне понимаю, что данным сообщением вызову дополнительный интерес, но хочу сразу предостеречь пытливых - стоп. Остальные просто не найдут'
          },
          {
            title: 'Второй таб (среднестатистический)',
            icon: 'alert',
            notification: true,
            content: 'Ребята, давайте вскроем эту тему. Вы ведь уже не молодые, дела даются все сложнее и сложнее. Это же мать Тереза, это Библия, сюда можно без опаски влезать. Серьезно, вы не пожалеете. Лучше еще раз перечитайте что тут написано чтобы не забыть. Я вполне понимаю, что данной писаниной вызываю у вас к делу отвращение, но хочу просто сказать ленивым - ВПЕРЕД! Остальные найдут это благодаря вашим стараниям',
            isSecondary: true
          },
          {
            title: 'Задизейбленный таб',
            isDisabled: true,
            content: 'Bernd you really should not open this topic. You are young, playful, everything\'s easy for you This is an another thing. It\'s not Snowden and not even secret services archives. Better not to get into that. Seriously, any of you would deeply regret. Close the topic and forget what was written here. I fully understand that this post incites additional interest, but I far the curious - stop. The rest will just not see it.'
          },
          {
            title: '6 таб',
            icon: 'alert',
            content: '5 The rest will just not see it. Что-то написано на английском.'
          },
          {
            title: 'Вкладка справа 1',
            content: 'Воу! Да это де вкладка справа!',
            isAlignRight: true
          },
          {
            title: 'Вкладка справа 2',
            content: 'И это тоже вкладка справа! Не может быть!',
            isAlignRight: true
          }
        ])
      }
    },
    template: `
      <div>
        <div style="background-color: #fff; margin: 20px 0 40px">
          <ObTabs 
            :buttonMargin="buttonMargin"
            :valueExp="valueExp"
            :itemsWithDivider="[2]"
          >
            <ObTabsPane
              v-for="(item, idx) in dataArray"
              :key="'tabs-pane-' + idx"
              :title="item.title"
              :notification="item.notification"
              :is-selected="item.isSelected"
              :is-disabled="item.isDisabled"
              :is-secondary="item.isSecondary"
              :is-hidden="item.isHidden"
              :is-align-right="item.isAlignRight"
              :icon="item.icon"
              :tooltip="item.tooltip"
            >
              <div style="padding: 20px">
                {{ item.content }}
              </div>
            </ObTabsPane>
          </ObTabs>


        </div>

        <div style="padding: 20px; background-color:#fff">
          <p>
            Если табы лежат в элементе с box-shadow, то нижний бордер необходимо убрать <br>
            Для этого есть пропс <code>no-border</code>
          </p>
          <br>
          <div style="box-shadow: 0px 4px 10px rgba(0, 75, 200, 0.06), 0px 0px 12px rgba(0, 75, 200, 0.04), 0px 0px 2px rgba(0, 75, 200, 0.15);">
            <ObTabs
              :items="[{ title: 'Таб 1' }, { title: 'Таб 2', isSecondary: true }, { title: 'Таб 3' }]"
              no-border
            />
          </div>
        </div>
        
        <ObTabs
          :items="[{ title: 'Таб 1', notification: true }, { title: 'Таб 2' }, { title: 'Таб 3' }]"
          @on-change-tab="e => {
            tabChangesCount++
            tabChangedMsg = e
          }"
        />

        <div style="padding: 20px;">
          <span v-if="tabChangesCount === 0">
            івенту ще не було(((
          </span>
          <span v-else>
            таб змінюється в {{ tabChangesCount }} раз!
            <br>
            поточне значення: {{ tabChangedMsg }}
          </span>
        </div>
      </div>
    `
  }),
  {
    info: {
      components: { ObTabs, ObTabsPane },
      summary: `
      \`\`\`html
        <ObTabs :items-with-divider="[2]">
          <ObTabsPane
            v-for="(item, idx) in [item1, item2, item3, item4, item5]"
            :key="'tabs-pane-' + idx"
            :title="item.title"
            :notification="item.notification"
            :is-selected="item.isSelected"
            :is-disabled="item.isDisabled"
            :is-secondary="item.isSecondary"
            :is-hidden="item.isHidden"
            :icon="item.icon"
            :tooltip="item.tooltip"
          >
            <div style="padding: 20px">
              {{ item.content }}
            </div>
          </ObTabsPane>
        </ObTabs>
      \`\`\`
      `,
      source: false
    }
  })
