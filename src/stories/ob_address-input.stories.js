/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObAddresModal from '../obComponents/obAddressModal'
import ObButton from '../obComponents/obButton/index.vue'

const stories = storiesOf('ObAddresModal', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    isPoint: {
      default: boolean('isPoint', false)
    },
    isMobile: {
      default: boolean('isMobile', false)
    },
    mobileTitle: {
      default: text('mobileTitle', 'Заголовок для мобильной версии')
    },
    mapId: {
      default: text('mapId', 'map-id')
    }
  },
  data () {
    return {
      location: { },
      dataSource: {}
    }
  },
  components: { ObAddresModal, ObButton },
  methods: {
    open () {
      this.$refs.modalMap.open()
    }
  },
  template: `
      <div class="element_container">
        <ObButton @click="open" label="Open modal"></ObButton>
        <ObAddresModal
            v-model="location"
            ref="modalMap"
            :mobileTitle="mobileTitle"
            :isMobile="isMobile"
            :mapId="mapId"
            :isPoint="isPoint"
            :data-source="{ address: dataSource }"
        />
        {{ location }}
      </div>`
}),
{
  info: {
    components: { ObAddresModal },
    summary: `
        \`\`\`html
        <ObAddresModal
            v-model="location"
            ref="modalMap"
            :mobileTitle="mobileTitle"
            :isMobile="isMobile"
            :mapId="mapId"
            :isPoint="isPoint"
            :data-source="{ address: dataSource }"
        />
        \`\`\`
      `,
    source: false
  }
}
)
