/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs'
import { required, minLength, maxLength, numeric, requiredIf, email } from 'vuelidate/lib/validators'
import ObForm from '../obComponents/obForm'
import FormBody from './ObFormStories'
import ObModal from '../obComponents/obModal'
import { withInfo } from 'storybook-addon-vue-info'

const stories = storiesOf('ObForm', module)

const checkItemTitle = (value, vm) => {
  // vm - текущий уровень модели
  return Boolean(value && value.length <= 5)
}

function checkItemsLength (value) {
  // this - инстанс компонента ObForm
  return Boolean(!this.formData || !this.formData.count || +this.formData.count === value.length)
}

const checkItemsCount = (value) => {
  // this - инстанс компонента ObForm
  return Boolean(!value || value <= 2)
}

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  components: {
    ObForm,
    FormBody,
    ObModal
  },
  props: {
    title: {
      default: text('title', 'Заголовок формы')
    },
    isReadOnly: {
      default: boolean('isReadOnly', false)
    },
    isOnlySubmit: {
      default: boolean('isOnlySubmit', false)
    },
    okText: {
      default: text('okText', 'Сохранить')
    },
    cancelText: {
      default: text('cancelText', 'Отмена')
    },
    isSubmitDisabled: {
      default: select('isSubmitDisabled', [ undefined, true, false ], undefined)
    },
    isShowControls: {
      default: boolean('isShowControls', true)
    },
    isShowRequired: {
      default: boolean('isShowRequired', true)
    },
    isOnPressEnterSubmit: {
      default: boolean('isOnPressEnterSubmit', false)
    },
    isActiveElement: {
      default: select('isActiveElement', [ null, true, false ], null)
    },
    btnModal: {
      default: text('btnModal', '')
    },
    isLoading: {
      default: select('isLoading', [ true, false ], false)
    },
    isCancelDisabled: {
      default: boolean('isCancelDisabled', false)
    }
  },
  data () {
    return {
      dataSource: {},
      formData: {
        name: '',
        count: '',
        detail: {
          title: '',
          items: []
        }
      },
      dropdownMenu: [
        {
          title: 'Действие 1',
          method: () => alert('Вы совершили действие 1')
        },
        {
          title: 'Действие 2',
          method: () => alert('Вы совершили действие 2')
        }
      ]
    }
  },
  computed: {
    formValidator () {
      return this.$refs.form && this.$refs.form.$v
    },
    formModel () {
      return {
        name: {
          value: '',
          validator: {
            required,
            minLength: {
              method: minLength(5),
              error: 'Слишком короткое имя'
            },
            maxLength: {
              method: maxLength(10),
              error: 'Слишком длинное имя'
            }
          }
        },
        count: {
          value: '',
          validator: {
            number: {
              method: numeric,
              error: 'Нужно ввести число'
            },
            maxLength: {
              method: checkItemsCount,
              error: 'Не больше двух элементов, парень'
            }
          }
        },
        detail: {
          nested_fields: {
            title: {
              value: '',
              validator: {
                required: requiredIf(function (model) {
                  // model - текущий уровень модели
                  // this - инстанс компонента ObForm
                  return this.formData.count && this.formData.count > 1
                })
              }
            },
            items: {
              type: 'array',
              value: [],
              validator: {
                checkItemsLength: {
                  method: checkItemsLength
                },
                $each: {
                  title: {
                    value: '',
                    validator: {
                      required,
                      maxLength: {
                        method: checkItemTitle,
                        error: 'Имя элемента не должно быть больше 5 символов'
                      }
                    }
                  },
                  email: {
                    value: '',
                    validator: {
                      required,
                      email: {
                        method: email,
                        error: 'Некорректный email'
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  template: `
  <div class="element_container">
    <div class="row">
      <div class="col-xs-6">
        <ObForm
          ref="form"
          :title="title"
          :is-read-only="isReadOnly"
          :dropdown-menu="dropdownMenu"
          :is-only-submit="isOnlySubmit"
          :is-show-controls="isShowControls"
          :ok-text="okText"
          :cancel-text="cancelText"
          :is-show-required="isShowRequired"
          :is-on-press-enter-submit="isOnPressEnterSubmit"
          :form-model="formModel"
          :form-data="formData"
          :is-active-element="isActiveElement"
          :btn-modal="btnModal"
          :is-cancel-disabled="isCancelDisabled"
          :is-loading="isLoading"
          @change-active="onChangeActive"
          @on-btn-modal="openModalChild"
        >
            <FormBody
              :form-data="formData"
            />
        </ObForm>
      </div>
    </div>
    <ObModal
        ref="ob-modal-child"
        size="small"
        :title="btnModal"
      >
        <p>{{ btnModal}}</p>
    </ObModal>
  </div>`,
  methods: {
    action: action('clicked'),
    openModalChild () {
      this.$refs['ob-modal-child'].open()
    },
    onChangeActive (val) {
      alert('is-active state: ' + val)
    }
  }
}),
{
  info: {
    components: { ObForm, FormBody },
    summary: `
        \`\`\`html
          <ObForm
          ref="form"
          :title="title"
          :is-read-only="isReadOnly"
          :dropdown-menu="dropdownMenu"
          :is-only-submit="isOnlySubmit"
          :is-show-controls="isShowControls"
          :ok-text="okText"
          :cancel-text="cancelText"
          :is-show-required="isShowRequired"
          :is-on-press-enter-submit="isOnPressEnterSubmit"
          :form-model="formModel"
          :form-data="formData"
          :is-active-element="isActiveElement"
          :btn-modal="btnModal"
          :is-cancel-disabled="isCancelDisabled"
          :is-loading="isLoading"
          @change-active="onChangeActive"
          @on-btn-modal="openModalChild"
        >
            <FormBody
              :form-data="formData"
            />
        </ObForm>
        \`\`\`
        `,
    source: false
  }
})
