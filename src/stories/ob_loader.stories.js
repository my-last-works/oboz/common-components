/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ObLoader from '../obComponents/obLoader/index.vue'
import { number, withKnobs } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'

storiesOf('ObLoader', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    props: {
      size: {
        default: number('size', 40)
      },
      delay: {
        default: number('delay', 4)
      }
    },
    components: { ObLoader },
    template: `
      <div class="element_container">
        <ObLoader :size="size" :delay="delay" />
      </div>
    `,
    methods: {
      clickToObject () {},
      onChangeLocale () {}
    }
  }),
  {
    info: {
      components: { ObLoader },
      summary: `
          \`\`\`html
            <ObLoader :size="size" :delay="delay" />
          \`\`\`
      `,
      source: false
    }
  })
