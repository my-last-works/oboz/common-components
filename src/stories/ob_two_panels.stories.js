/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { boolean, text, withKnobs } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'

import ObTwoPanels from '../obComponents/obTwoPanels'

storiesOf('ObTwoSection', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    data () {
      return {
        menu: []
      }
    },
    props: {
      leftCaption: {
        default: text('leftCaption', 'Левый заголовок')
      },
      rightCaption: {
        default: text('rightCaption', 'Правый заголовок')
      },
      minLeftWidth: {
        default: text('minLeftWidth', '100')
      },
      heightFull: {
        default: boolean('heightFull', false)
      }
    },
    computed: {},
    components: { ObTwoPanels },
    template: `
        <ObTwoPanels
            :leftCaption="leftCaption"
            :rightCaption="rightCaption"
            :minLeftWidth="minLeftWidth"
            :heightFull="heightFull"
        >
            <template slot="left">
                2.2
            </template>
            <template slot="right">
                1<br/>
                1<br/>
            </template>
        </ObTwoPanels>
    `,
    methods: {
      clickToObject () {
      },
      onChangeLocale () {
      }
    }
  }),
  {
    info: {
      components: { ObTwoPanels },
      summary: `
      \`\`\`html
        <ObTwoPanels>
            <template slot="left-header">
                Монитор задач
            </template>
            <template slot="left">
                2.2
            </template>
            <template slot="right-header">
                Задача
            </template>
            <template slot="right">
                1<br/>
                1<br/>
            </template>
        </ObTwoPanels>
      \`\`\`
      `,
      source: false
    }
  })
