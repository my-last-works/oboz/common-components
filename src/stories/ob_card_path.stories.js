/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean, color } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import ObCardPath from '../obComponents/obCardPath'

const stories = storiesOf('ObCardPath', module)

stories.addDecorator(withKnobs)
stories.addDecorator(withInfo)
stories.add('sample', () => ({
  props: {
    showLabel: {
      default: boolean('showLabel', true)
    },
    labelText: {
      default: text('labelText', 'Самый дешевый')
    },
    labelColor: {
      default: color('labelColor', '#10CF96')
    },
    labelIcon: {
      default: text('labelIcon', 'clock')
    },
    tariffByHand: {
      default: boolean('tariffByHand', true)
    }
  },
  data () {
    return {
      itemsDots: {
        price: '58 000',
        contractor: '',
        startDate: '24 августа',
        endDate: '26 августа',
        routeLength: '818 км',
        routeDuration: '1 д 12 ч',
        vatInPercents: '20',
        dots: [
          {
            icon: 'city',
            title: 'Москва',
            type: 'dotLocation',
            date: '',
            dots: ['from', 'from', 'from']
          },
          {
            icon: 'tractor-semi-trailer-izoterm-g',
            title: 'Изотерм. 20 т / 68 м3 / 33 пал ',
            type: 'dotAuto',
            count: 1
          },
          {
            icon: 'city',
            title: 'Самара',
            type: 'dotLocation',
            date: '',
            dots: ['to', 'to']
          }
        ]
      }
    }
  },
  components: { ObCardPath },
  template: `        
      <div style='padding:20px;'>
        <div>
          <ObCardPath :dots="itemsDots" :showLabel="showLabel" :labelText="labelText" :labelColor="labelColor" :labelIcon="labelIcon" :tariffByHand="tariffByHand"/>
        </div>
      </div>`,
  methods: {}
}),
{
  info: {
    components: { ObCardPath },
    summary: `
        \`\`\`html
          <ObCardPath 
            :dots="itemsDots" 
            :showLabel="showLabel" 
            :labelText="labelText" 
            :labelColor="labelColor" 
            :labelIcon="labelIcon" 
            :tariffByHand="tariffByHand"
          />
        \`\`\`
        `,
    source: false
  }
})
