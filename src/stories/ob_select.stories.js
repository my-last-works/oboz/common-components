
import { storiesOf } from '@storybook/vue'

import ObSelector from '../obComponents/obSelect/index.vue'
import ObCheckbox from '../obComponents/obCheckbox/index.vue'
import ObInput from '../obComponents/obInput/index.vue'
import ObStepper from '../obComponents/obStepper/index.vue'
import { withInfo } from 'storybook-addon-vue-info'
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs'

storiesOf('ObSelect', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => ({
    props: {
      value: {
        default: text('value', '')
      },
      label: {
        default: text('label', 'Куратор сделки')
      },
      placeholder: {
        default: text('placeholder', 'Select')
      },
      displayExp: {
        default: text('displayExp', 'title')
      },
      valueExp: {
        default: text('valueExp', 'uuid')
      },
      subTitleExp: {
        default: text('subTitleExp', 'subtitle')
      },
      pattern: {
        default: text('pattern', '')
      },
      position: {
        default: text('position', 'bottom')
      },
      property: {
        default: text('property', null)
      },
      disabled: {
        type: Boolean,
        default: boolean('disabled', false)
      },
      isAutoComplete: {
        type: Boolean,
        default: boolean('isAutoComplete', false)
      },
      isTagbox: {
        type: Boolean,
        default: boolean('isTagbox', false)
      },
      isDropdown: {
        type: Boolean,
        default: boolean('isDropdown', false)
      },
      isCompactDropdown: {
        type: Boolean,
        default: boolean('isCompactDropdown', false)
      },
      isOnlyDropdown: {
        type: Boolean,
        default: boolean('isOnlyDropdown', false)
      },
      withCheckbox: {
        type: Boolean,
        default: boolean('withCheckbox', false)
      },
      invalid: {
        type: Boolean,
        default: boolean('invalid', false)
      },
      isStandart: {
        type: Boolean,
        default: boolean('isStandart', false)
      },
      isInput: {
        type: Boolean,
        default: boolean('isInput', false)
      },
      isSearch: {
        type: Boolean,
        default: boolean('isSearch', false)
      },
      isClearable: {
        type: Boolean,
        default: boolean('isClearable', false)
      },
      required: {
        type: Boolean,
        default: boolean('required', false)
      },
      showItemsCount: {
        default: number('showItemsCount', 5)
      },
      pageLimit: {
        default: number('pageLimit', 10)
      },
      count: {
        default: number('count', 10)
      },
      labelInfo: {
        default: text('labelInfo', 'Текст для подсказки')
      }
    },
    components: { ObSelector, ObCheckbox, ObInput, ObStepper },
    data () {
      return {
        valueLocal: '',
        valueGroups: ''
      }
    },
    computed: {
      itemsWrapper () {
        return [
          {
            title: 'Справка об отсутствии задолж-ти',
            uuid: 'Value 1',
            icon: 'alert'
          },
          {
            title: 'Placeholder text 1',
            subtitle: 'Correct',
            uuid: 'Placeholder 1'
          },
          {
            title: 'Нижний Новгород, Россия',
            uuid: 'Value 3',
            icon: 'city-sm'
          },
          {
            title: 'Title 4',
            uuid: 'Value 4'
          },
          {
            title: 'Title 5',
            uuid: 'Value 5'
          },
          {
            title: 'Title 6',
            uuid: 'Value 6',
            disabled: true
          },
          {
            title: 'Title 7',
            uuid: 'Value 7'
          },
          {
            title: 'Title 8',
            uuid: 'Value 8',
            disabled: true
          },
          {
            title: 'Title 9',
            uuid: 'Value 9',
            disabled: true
          },
          {
            title: 'Title 10',
            uuid: 'Value 10'
          },
          {
            title: 'Title 11',
            uuid: 'Value 11'
          },
          {
            title: 'First group',
            uuid: 'Value 12',
            children: [
              {
                title: 'First group - child 1',
                uuid: 'First group - child 1'
              },
              {
                title: 'First group - child 2',
                uuid: 'First group - child 2'
              }
            ]
          },
          {
            title: 'Second group',
            uuid: 'Value 13',
            hideSelectButton: true,
            children: [
              {
                title: 'Second group - child 1',
                uuid: 'Second group - child 1'
              },
              {
                title: 'Second group - child 2',
                uuid: 'Second group - child 2'
              }
            ]
          }
        ]
      },
      groupItems () {
        return [
          {
            title: 'Группа 1',
            defaultGroupItemUuid: 'value 100',
            children: [
              {
                title: 'Справка об отсутствии задолж-ти',
                uuid: 'Value 100',
                icon: 'alert'
              },
              {
                title: 'Placeholder text 1',
                subtitle: 'Correct',
                uuid: 'Value 101'
              },
              {
                title: 'Нижний Новгород, Россия',
                uuid: 'Value 102',
                icon: 'city-sm'
              }
            ]
          },
          {
            title: 'Группа 2',
            defaultGroupItemUuid: 'Value 103',
            children: [
              {
                title: 'Справка об отсутствии задолж-ти',
                uuid: 'Value 103',
                icon: 'alert'
              },
              {
                title: 'Placeholder text 1',
                subtitle: 'Correct',
                uuid: 'Value 104'
              },
              {
                title: 'Нижний Новгород, Россия',
                uuid: 'Value 105',
                icon: 'city-sm'
              }
            ]
          }
        ]
      }
    },
    template: `
      <div class="element_container">
        <ObSelector
          @bottom="handleItemBottomClick"
          :required="required"
          :isInput="isInput"
          :isSearch="isSearch"
          :isStandart="isStandart"
          :is-clearable="isClearable"
          :property="property"
          :invalid="invalid"
          :count="count"
          :pageLimit="pageLimit"
          :withCheckbox="withCheckbox"
          :showItemsCount="showItemsCount"
          :isDropdown="isDropdown"
          :isCompactDropdown="isCompactDropdown"
          :isTagbox="isTagbox"
          :isAutoComplete="isAutoComplete"
          :isOnlyDropdown="isOnlyDropdown"
          :disabled="disabled"
          :placeholder="placeholder"
          :label="label"
          :labelInfo="labelInfo"
          :valueExp="valueExp"
          :displayExp="displayExp"
          :subTitleExp="subTitleExp"
          :itemBottom="{ icon: 'plus', title: 'Добавить грузоотправителя' }"
          :items="itemsWrapper"
          v-model="valueLocal"
          :pattern="pattern"
          :position= "position"
          style="margin-top: 20px;" 
        />
        <br/>
        <br/>
        <br/>
        <h2>Выпадающий список с группировкой</h2>
        <ObSelector
          @bottom="handleItemBottomClick"
          :required="required"
          :isInput="isInput"
          :isSearch="isSearch"
          :isStandart="isStandart"
          :is-clearable="isClearable"
          :property="property"
          :invalid="invalid"
          :count="count"
          :pageLimit="pageLimit"
          :withCheckbox="withCheckbox"
          :showItemsCount="showItemsCount"
          :isDropdown="isDropdown"
          :isCompactDropdown="isCompactDropdown"
          :isTagbox="isTagbox"
          :isAutoComplete="isAutoComplete"
          :isOnlyDropdown="isOnlyDropdown"
          :disabled="disabled"
          :placeholder="placeholder"
          :label="label"
          :labelInfo="labelInfo"
          :valueExp="valueExp"
          :displayExp="displayExp"
          :subTitleExp="subTitleExp"
          :itemBottom="{ icon: 'plus', title: 'Добавить грузоотправителя' }"
          :items="groupItems"
          v-model="valueGroups"
          :pattern="pattern"
          :position= "position"
          style="margin-top: 20px;"
        />
      </div>
    `,
    mounted () {
      setTimeout(() => {
        this.items3 = [
          {
            title: 'Title 1',
            uuid: 'Value 1',
            icon: 'alert'
          }
        ]
      }, 1000)
    },
    methods: {
      handleItemBottomClick () {}
    }
  }),
  {
    info: {
      components: { ObSelector },
      summary: `
      \`\`\`html
        <ObSelector
          :items="dataItems"
          v-model="fieldValue"
          label="Наименование"
          placeholder="Выберите значение"
        />
      \`\`\`
      `,
      source: false
    }
  })
