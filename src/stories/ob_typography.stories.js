/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

storiesOf('ObTypography', module)
  .add('sample', () => ({
    data () {
      return {
        label: 'Секреты',
        disabled: false,
        dark: false,
        danger: false,
        mini: false,
        type: 'primary',
        demoCounter: 0
      }
    },
    template: `<div>
        <div style="width:50%; float:left;">
      	<p class="ob-ty-h-0">Header 0</p>
      	<p class="ob-ty-h-1">Header 1</p>
      	<p class="ob-ty-h-2">Header 2</p>
      	<p class="ob-ty-h-3">Header 3</p>
      	<p class="ob-ty-b-1">Body 1</p>
      	<p class="ob-ty-b-2">Body 2</p>
      	<p class="ob-ty-label">Label</p>
      	<p class="ob-ty-placeholder">Placeholder</p>
      	<p class="ob-ty-button">Button</p>
      	<p class="ob-ty-button-i">Icon button</p>
      	<p class="ob-ty-table-label">Table label</p>
      	<p class="ob-ty-modal-title">Modal title</p>
      	</div>
      	      <div style="width:50%; float:left;">
      	<ol class="ob-ty-ol">
      		<li>Ordered list</li>
      		<li>Ordered list</li>
      		<li>Ordered list</li>
      		<li>Ordered list</li>
				</ol>
				<ul class="ob-ty-ui">
      		<li>Unordered list</li>
      		<li>Unordered list</li>
      		<li>Unordered list</li>
      		<li>Unordered list</li>
				</ul>
      	<ol class="ob-ty-ol -mini">
      		<li>Ordered list</li>
      		<li>Ordered list</li>
      		<li>Ordered list</li>
      		<li>Ordered list</li>
				</ol>	
				<ul class="ob-ty-ui -mini">
      		<li>Unordered list</li>
      		<li>Unordered list</li>
      		<li>Unordered list</li>
      		<li>Unordered list</li>
				</ul>
				</div>		
      </div>`,
    methods: { action: action('clicked') }

  }))
