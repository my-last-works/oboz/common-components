import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'
import { withInfo } from 'storybook-addon-vue-info'
import AddressInput from '../obComponents/obAddressInput'

storiesOf('ObAddressInput', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('sample', () => {
    return {
      components: { AddressInput },
      props: {
        address: {
          type: String,
          default: text('address', 'Мосфильмовская ул., 17, Москва, Россия, 119590')
        },
        showMap: {
          type: Boolean,
          default: boolean('showMap', true)
        },
        isLocations: {
          type: Boolean,
          default: boolean('isLocations', false)
        },
        isMobile: {
          type: Boolean,
          default: boolean('isMobile', false)
        },
        customIcon: {
          type: Boolean,
          default: text('customIcon', 'edit')
        },
        required: {
          type: Boolean,
          default: text('required', 'edit')
        },
        isAddressOnly: {
          type: Boolean,
          default: boolean('isAddressOnly', false)
        },
        disabled: {
          type: Boolean,
          default: boolean('disabled', false)
        },
        placeholder: {
          type: String,
          default: text('placeholder', 'Адрес или точка на карте')
        },
        title: {
          type: String,
          default: text('title', 'Откуда')
        },
        isValid: {
          type: Boolean,
          default: boolean('isValid', true)
        },
        isLocationsOnly: {
          type: Boolean,
          default: boolean('isLocationsOnly', false)
        },
        popoverMessage: {
          type: String,
          default: text('Error message for invalid address', 'Popover message')
        },
        prefixIcon: {
          type: String,
          default: text('Prefix Icon', 'pin')
        }
      },

      data () {
        return {
          addressFrom: {
            russian_address: 'Мосфильмовская ул., 17, Москва, Россия, 119590',
            coordinates: { lat: 55.71512985, lon: 37.51725006 }
          },
          mapAddress: {},
          locationsTypes: [{ 'uuid': 'bbb964b2-b799-11e9-a2a3-2a2ae2dbcce4', 'infrastructure_facility': true, 'data_completion': 'Автоматически', 'name': 'Аэропорт', 'description': 'Аэропорт', 'is_active': true }, { 'uuid': '4adf706e-0be2-4383-b680-a73c959b9e2c', 'infrastructure_facility': true, 'data_completion': 'Автоматически', 'name': 'ЖД станция', 'description': 'ЖД станция', 'is_active': true }, { 'uuid': '4adf706e-0be2-4383-b680-a73c959b9e4c', 'infrastructure_facility': true, 'data_completion': 'Автоматически', 'name': 'Контейнерный терминал', 'description': 'Контейнерный терминал', 'is_active': true }, { 'uuid': '4adf706e-0be2-4383-b680-a73c959b9e2f', 'infrastructure_facility': true, 'data_completion': 'Автоматически', 'name': 'Морской Порт', 'description': 'Морской Порт', 'is_active': true }, { 'uuid': '5919a99f-d840-4855-b158-5ba957076603', 'infrastructure_facility': false, 'data_completion': 'Вручную', 'name': 'Опорная точка', 'description': 'Опорная точка1', 'is_active': true }, { 'uuid': 'a2166b4d-27a9-41ce-9fc8-8bfd21baa8d7', 'infrastructure_facility': true, 'data_completion': 'Автоматически', 'name': 'Пограничный переход', 'description': 'Пограничный переход', 'is_active': true }, { 'uuid': '4adf706e-0be2-4383-b680-a73c959b9e3c', 'infrastructure_facility': true, 'data_completion': 'Автоматически', 'name': 'Пункт таможенного оформления/СВХ', 'description': 'Пункт таможенного оформления/СВХ', 'is_active': true }]
        }
      },

      methods: {
        setAddress ($event) {
          this.addressFrom.russian_address = $event.label
          this.addressFrom.coordinates = $event.coordinate
        }
      },

      template: `
        <div class="element_container">
        <AddressInput
          :title="title"
          :address="address"
          :mapAddress="mapAddress"
          :showMap="showMap"
          :isValid="isValid"
          :isLocationsOnly="isLocationsOnly"
          :locationsTypes="locationsTypes"
          :popoverMessage="popoverMessage"
          :prefixIcon="prefixIcon"
          :placeholder="placeholder"
          :disabled="disabled"
          :isLocations="isLocations"
          :isMobile="isMobile"
          :customIcon="customIcon"
          :isAddressOnly="isAddressOnly"
          :required="required"
          display-exp="russian_address"
          mapId="map-2"
          @input="setAddress"
          @on-change-address="setAddress"
        />
      </div>
      `
    }
  },
  {
    info: {
      components: { AddressInput },
      summary: `
        \`\`\`html
          <AddressInput
            :title="title"
            :address="address"
            :mapAddress="mapAddress"
            :showMap="showMap"
            :isValid="isValid"
            :isLocationsOnly="isLocationsOnly"
            :locationsTypes="locationsTypes"
            :popoverMessage="popoverMessage"
            :prefixIcon="prefixIcon"
            :placeholder="placeholder"
            :disabled="disabled"
            display-exp="russian_address"
            mapId="map-2"
            @input="setAddress"
            @on-change-address="setAddress"
          />
        \`\`\`
      `,
      source: false
    }
  })
