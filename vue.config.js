const path = require('path')

module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `@import "@/static/css/variables.scss";`
      }
    }
  },
  configureWebpack: {
    performance: {
      hints: false
    },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000
      }
    },
    resolve: {
      alias: {
        '@css': path.resolve(__dirname, 'src/static/css/'),
        '@mixins': path.resolve(__dirname, 'src/static/css/mixins'),
        '@vars': path.resolve(__dirname, 'src/static/css/variables')
      },
      extensions: ['.js', '.scss']
    },
    devtool: process.env.NODE_ENV !== 'production' ? 'eval-source-map' : 'none'
  }
}
