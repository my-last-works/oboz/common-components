# Общие принципы

В данном репозитории находятся vue-компоненты и вспомогательные функции для использования в фронтенд-приложениях проектов oboz2 и seal(АКА Пломба).

При работе с данным репозиторием необходимо иметь в виду, что данный код влияет на несколько компонентов, а посему при внесении изменений следует:

1. Согласовывать с другими разработчиками изменения перед внесением
2. При внесении изменений оставлять обратную совместимость с существующим поведением
3. При невозможности сохранения совместимости необходимо **сначала** внести измения в зависимые проекты, а _затем_ добавлять изменения в этой библиотеке

# Использование Storybook

Для демонстрации компонентов в этом репозитории используется `storybook`, чтобы воспользоваться сторибуком используйте команду: `npm run storybook:dev`

# Другие способы отладки компонентов

Для отладки Layout'а, Datagrid, модалов удобнее пользоваться dummy-приложением. Чтобы запустить его, используйте: `npm run dev`

# API

## Компоненты

### Layout

Обрамление, используемое во всех проектах

#### Layout
 ```html
   <Layout
     :top-level-members="topLevelMembers"
     @logout="logoutFunction">
     <router-view/>
   </Layout>

 ```
  В общем случае этот компонент встраивается в корневой компонент  `src/App.vue`  или аналогичный.

  У компонента существует один дефолтный слот, куда складывается контент страницы, или, в случае с `vue-router` `<router-view />`

  Вот пропы, которые передаются в этот компонент:

  * `top-level-members` - массив, описывающий структуру верхнего меню и вложенных компонентов среднего Меню
     ```javascript
     const topLevelMembers = [
       { //  некоторые заголовки могут указывать на ресурсы за пределами
         caption: 'Заказы',
         href: '/orders',
         only: ['moderator', 'client', 'urmom'] //если указан user-role, то если роль не указана в этом массиве, пункт меню будет отключен
       },
       {  //  другие могут переадресовывать в компоненты приложения
         caption: 'Пломба',
         to: {name: 'Test'},
         disabled: true, //делает меню неактивным
         members: [ // в этой ситуации необходимо передать пункты меню
           { // пункты без ссылки воспринимаются как заголовки
             title: 'Администрирование'
           },
           { // вертикальная черта
             border: true
           },
           { // линки к роутам vue-router
             title: 'Организации',
             to: {name: 'Contractors'}
           }
           // TODO: добавить возможность навигации на внешние ресурсы из внешнего меню
         ]
       },
       { //двухуровневое меню выглядит так
         caption: 'Справочники', //обратите внимание - не нет ни ключа to, ни ключа href
         members: [
           {
             caption: 'Роли', //здесь используется поле caption, а не title
             href: '/somename'
           },
           {
             caption: 'Организации',
             to: { name: 'Test' },
             only: ['moderator', 'client', 'urmom'],
             members: [
               {
                 title: 'Роewли',
                 to: {name: 'Home'},
                 only: ['moderator', 'client', 'urmom']
               },
               {
                 title: 'Организewации',
                 to: {name: 'Contractors'}
               },
               {
                 title: 'Достуewпы',
                 to: {name: 'Access'}
               }
             ]
           },
         ]
     ];
     ```
  * `username` -  строка, имя пользователя для отображения
  * `role` - строка, роль пользователя для отбражения
  * `role-icon` - строка, название роли пользователя для отображения, опциональна. Осмысленные значения: `administrator`, `carrier`, `expeditor`, `agent`, `client`, `moderator`
  * `company` - строка, компания пользователя для отображения
  * `companies` - объект, где ключи - идентификаторы компаний, а значения - объекты с названиями и фичами для отображения(фичи будут определены позже)
    ```javascript
      {
        horns_n_hooves_limited: {
          name: 'ООО Рога и копыта',
          features: []
        },
        qwerty_inc: {
          name: 'ОАО Йцукен',
          features: ['pinned']// так будут подаваться разные фичи
        }
      }
    ```
  * `tasks-count` - число задач
  * `tasks-to` - объект или строка, если объект, то ведет себя как атрибут `to` роутер-линка, если строка - как обычная ссылка. Активируется по нажатию гамбургера задач.
  * `tasks-active` - булева переменная, подсвечивает активным гамбургер задач
  * `user` - объект, передается, только если баланс - актуальный параметр для юзера
     ```javascript
      {
        balance: 1000
      }
     ```
  * `user-role` - строка или массив строк, используется для отключения пунктов меню по ключу `only`
  * `version` - строка, версия и дата для отбражения внизу справа, если не указано дефолтится на `Без версии <текущая дата>`


  А это события, излучаемые этим компонентом:

  * `logout` - событие излучается при нажатии кнопки Выход  в интефейсе
  * `settings` - событие излучается при нажатии кнопки Настройки
  * `profile` - событие излучается при нажатии кнопки Профиль
  * `company`  - событие излучается при нажатии кнопки сгенерированной из пропа `companies`, вместе с событием излучается строковая пременная соответствующая ключу соответствующей записи в объекте `companies`

#### SinglePane
  ```html
    <single-pane capt="Блок">
      <template v-slot:header>
         Header
      </template>
      Content
    </single-pane>
  ```
  Данный компонент используется в качестве корневого контейнера в компненте-странице(то есть том, который находится в папке  `/pages`) по умолчанию

  У компонента есть два слота:
  * дефолтный слот - сюда помещается основной контент страницы
  * `header` - слот заголовка, использование которого с датагридом теперь deprecated

  Пропы:
  * `capt` - строка, заглавие раздела

#### DoublePanes
  ```html
    <two-panes leftCaption="Лево" rightCaption="Право">
      <template v-slot:left-header>
        <span> Левый заголовок </span>
      </template>
      <template v-slot:left>
        <span> Левый контент </span>
      </template>
      <template v-slot:right-header>
        <span> Правый заголовок </span>
      </template>
      <template v-slot:right>
        <span> Правый контент </span>
      </template>
    </two-panes>
  ```
  Данный компонент используется в качестве корневого контейнера в компненте-странице(то есть том, который находится в папке  `/pages`), когда экран разделен на две половины

  У компонента есть четыре слота:
  * `left` - сюда помещается основной контент левой половины страницы
  * `left-header` - слот заголовка левой половины страницы, использование которого с datagrid теперь deprecated
  * `right` - сюда помещается основной контент правой половины страницы страницы
  * `right-header` - слот правой половины заголовка, использование которого с datagrid теперь deprecated
  * дефолтный слот **не используется**

  Пропы:
  * `leftCaption` - строка, заглавие левого раздела
  * `rightCaption` - строка, заглавие правого раздела

### Datagrid и его дочерние компоненты

 В oboz используются компоненты  devextreme, в особенности, для отображения таблиц мы используем  dx-data-grid. Datagrid - wrapper вокруг dx-data-grid, допиленный для проектов oboz2 и пломба.

#### Datagrid
  ```html
    <data-grid
      :columns="columns"
      :source="sealList"
      :add-button="true"
      @add="onAdd"
      :data-grid-ref-name="'sealsGrid'"
      :hide-functional-grid-panel="true"
      :keyExprInitial="propertyId"
      :remote-operations="remoteOperations"
      :master-detail="true"
      :open-first-master-row="true"
      @selected="onSelection"
    >
      <template
        slot="master-detail-template"
        slot-scope="{ value }"
      >
        <span>Возможность включения функции master/details</span>
      </template>
      <div
              slot="domain-settings-icon"
              slot-scope="{ value }"
      >
        <span>возможность подключения slot для ячеек</span>
      </div>


      <div slot="footer">
        <span>11 подключений</span>
        &nbsp;|&nbsp;
        <span>7 активных подкючений</span>
      </div>
    </data-grid>
  ```

  У компонента есть  один слот:
  * `footer` - контент футера, обычно там помещается `span` c информацией о коллекции, как то количество записей в коллекции
  * дефолтный слот **не используется**

  Компоненту можно передать следующие пропы:

  * `hideSortingSetting` - булева переменная, скрывает кнопку сортировки
  * `hideFunctionalGridPanel` - булева переменная, скрывает FunctionalGridPanel
  * `hideGroupingSetting` - булева переменная, скрывает кнопку группировки
  * `addButton` - булева переменная, скрывает кнопку добавления
  * `propertyId` - _не используемый проп_ TODO: актуализировать
  * `selectedRowKeysArray` - массив индексов предварительно выбранных строчек датагрида
  * `dataGridRefName` - строка, уникальная для датагрида среди отображенных элементов
  * `keyExprInitial` - строка или массив строк, задает то поле входных данных, которое будет использоваться как уникальный идентификатор при дальнейших манипуляциях
  * `masterDetail` - булева переменная, включение функции master/detail у компонента
  * `openFirstMasterRow` - булева переменная, первая строка при первом запуске становится открытой
  * `source` - массив объектов, представляющих отображаемые данные (без пагинации на сервере); `CustomStore` - при пагинации на сервере
  * `columns` - массив объектов, описывающий структуру колонок таблицы, подробно смотреть в документации к dx-data-grid, поле columns
    ```javascript
    [
      {
        title: 'Номер устр.',
        field: 'code',
        visible: isVisibleComputedProperty
      },
      {
        title: 'Наблюдатели',
        field: 'observers',
        calculateCellValue(data) {
          if (data.observers && data.observers.length) {
            return data.observers.map(item => item.name)
              .join(', ');
          }
          return '';
        }
      },
      {
        title: 'Статус',
        field: 'status',
        format: {
            cellTemplateName: 'domain-settings-icon', // наименование элемента внутри <DataGrid>, которое отвечает за отображение ячейки
            groupCellTemplateName: '',  // наименование элемента внутри <DataGrid>, которое отвечает за отображение группированной ячейки
        }
      },
    ]
    ```
  * `pagingOpt` - объект, содержит дополнительные опции для опции paging датагрида, передача пустого объекта `{}` включает paging без опций. Если проп не передается, пэйджинг не включается
  * `prevCount` - число, необходимо для скроллинга по данным в дата-грид модалом(TODO: actualize and refactor)
  * `nextCount` - число, необходимо для скроллинга по данным в дата-грид модалом(TODO: actualize and refactor)
  * `selectionOpt` - объект, содержит дополнительные опции для опции selection датагрида
  * `rowPrepared` - функция, вызывается при срабатывании коллбэка  onRowPrepared dx-data-grid,  то есть после создания новой строки в таблице
  * `remoteOperations` - объект, описывающий какие операции обработки данных происходят на сервере, `{paging: true}` включает пагинацию на сервере. Если включена пагинация на сервере, то данные для DataGrid `source` должны быть переданы следующим образом:
    ```javascript
    import CustomStore from "devextreme/data/custom_store";
    source = {
              store: new CustomStore({
                  load: function(loadOptions) {
                      // если не задан pageSize в pagingOpt по дефолту он 20 для data-grid
                      let page = loadOptions.skip/20;
                      return axios.get('https://dev.oboz.app/oboz2-dictionary-etsng-viewer/v1/cargo-nomenclature?page=' + page + '&size=' + loadOptions.take)
                          .then(({data}) => {
                              return {
                                  data: data.content,
                                  totalCount: data.totalElements
                              }
                          })
                          .catch(function (error) {
                              // handle error
                              console.log(error);
                          })
                          .finally(function () {
                              // always executed
                          });
                  }
              })
          };
    ```

  * При пагинации на сервере поиск работает тоже на сервере, тогда в запросе нужно указать поисковую строку, для получения строки из dataGrid нужно указать  `v-model="searchValue"` и использовать переменную в запросе

  * События, излучаемые компонентом:
     * ref - возвращает реф датагрида, излучается когда компонент mounted
     * option-changed - прокидывается событие optionChanged датагрида, см. https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxDataGrid/Events/#optionChanged
     * selection-changed - прокидывается событие selectionChanged датагрида, см. https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxDataGrid/Events/#selectionChanged

#### FunctionalGridPanel

Используется для интеграции фильтров и группировок в существующие проекты, где заменять dx-data-grid на наш компонент затратно.

TODO: refactor interaction with dataGrid

### Инпуты


#### ObInput
  ```html
   <ob-input
     :label = 'Label text'
     :placeholder = 'Enter please'
     v-model = 'name'
     :disabled = 'false'
     :readonly = 'false'
     :valid = 'true'
     :invalid = 'false'
     :message = '`This is the message`'
    >

   </ob-input>
  ```

  Пропы:
  * label - строка, название параметра в инпуте
  * placeholder - строка, текст отображаемый до ввода
  * disabled - булева переменная, делает инпут неактивным
  * readonly - булева переменная, делает инпут доступным
  * valid - булева переменная, делает инпут позитивно валидным
  * invalid - булева переменная, делает инпут позитивно ошибочным
  * message - строка, текст сообщения при наведении на иконку
  * value - строка, значение контролируемой переменной
  * type - строка, тип инпута под капотом, по умолчанию - текст(изменение данного параметра не оттестированно)
  * icon - строка, альтернативная иконка
  * iconized - булева переменная, включение/отключение


  События:
  * input - стандатное событие при вводе


  Слотов нет

### Форма

Форма с контролами, валидацией и контролами.

#### Использование

```javascript
import FormValidationMixin from 'common-components/src/components/form/FormValidationMixin.vue'
```

В миксине уже присутствует `formData` в дата, при монтировании и показе окна генерируется по `formModel`, определенной в компоненте
и также туда вливаются данные из `dataSource` - props из миксина (важно соблюдение структуры `formModel` и `dataSource`)

#### ObForm

```html
<ObForm :formData="formData" :formModel="formModel" :ref="formRefName" :read-only="true">
    ...
</ObForm>
```

`:ref="formRefName"` - временно обязательная строка (для обращения к компоненту)

*контролы*

Кнопки реагируют на валидацию и умеют работать с модальным окном.
Через computed ищется оборачивающий модал с name `ObForm` и используются его методы. Если не найдет есть `events` для обработки

*props*

* `formData` - объект данных, который наполняем (в компоненте также храним в ключе formData)
* `formModel` - модель нашей формы

    Модель выполняет сразу несколько функций:
    - Непосредственно описание модели данных
    - Формирование валидной структуры данных для отправки на основании заполненных данных в formData ([generateFormData](#generateFormData)).
    - Обогащение formData существующими данными ([generateFormData](#generateFormData)).

    `!!!Важно`
    1. Вложенные свойства объявляются через ключ nested_fields (нужно было для проверок и сравнений)
    2. Для свойств, отличных от строки желательно указать тип (далеее есть в примере).
    2. Само ядро валидатора можно посмотреть тут https://vuelidate.netlify.com
    
    Пример модели
    ```javascript
      import MODELS from '@/models';

      import {minLength, required, requiredIf} from "vuelidate/lib/validators";
        
    
      const isLongerStatus = (value, vm) => {
          // vm - текущий уровень даты
          // this.formData - корень, доступен через this, так как вызывается из компонента ObForm
          return value && vm.status && vm.status.text ? vm.status.text.length < value.length : false
      }
   

      const TEST = {
        title: {
          value: null,
          validator: {
            required: { // Вариант объявления правил с описанием ошибки 
              method: requiredIf(function (model) {
                return model && model.type === 2;
              }),
              error: 'Вы выбрали неверный тип, получите ошибку'
            }
          }
        },
        type: {
          value: null,
          validator: { // Простой вариант объявления правил
            required
          }
        },
        description: {
          value: null,
          validator: {
            required,
            minLength: {
              isLongerStatus
            }
          }
        },
        status: {
          transform: {
            byField: 'text',
            suffix: false // default this.byField (status__text)
          },
          nested_fields: {
            text: {
              value: null,
              validator: {
                required,
                minLength: {
                  method: minLength(5),
                  error: 'Нужно 10 буков'
                }
              }
            }
          }
        },
        notes: {
          type: 'array', // Boolean, Array, Number
          value: [],
          validator: {
            required: {
              method: requiredIf(function () {
                return this.formData.type === 1;
              }),
              error: 'Добавьте заметку'
            },
            $each: {
              title: {
                value: '',
                validator: {
                  required
                }
              }
            }
          }
        }
      }
    ```

* `readOnly` - режим только на просмотр - в стандартной панели остается только кнопка "Закрыть" (по умолчанию: false)

*events*

* `submit` - отправка формы `this.formData`
* `cancel` - нажатие кнопки "Отмена"

#### FormItemMixin

Интегрировано во все компоненты форм, чтобы заработали проверки, форма должна быть обернута в `<ObForm>` с корректными параметрами

*props*

* `property` - свойство, за которым следим, пока поддерживается только тип String.

Указываем в формате
    ```javascript
      title
    ```
    или
    ```javascript
      properties.control_date_from
    ```
    или
    ```javascript
      notes.[0].title
    ```

## Хэлперы

### formatNumber
```javascript
import { formatNumber } from 'common-components/src/helpers/common'
formatNumber(this.periodValue * this.tariffValue, { dot: 2 })
formatNumber(data.max_level_limit, { signed: true })
formatNumber(data.max_level_limit)
```
Возвращает число, подготовленное к выводу
TODO: больше примеров



### sample
```javascript
import { sample } from 'common-components/src/helpers/common'

sample([1, 3, 7, 66])
```

возавращает один случайный элемент массива

### shuffle
```javascript
import { shuffle } from 'common-components/src/helpers/common'

shuffle([1, 3, 7, 66])
```

возвращает одну случайную перестановку массива, поданного на вход

### rangeStep
```javascript
import { rangeStep } from 'common-components/src/helpers/common'

rangeStep(1, 1, 10) // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
rangeStep(100, 10, 200) // [100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200]
```
 возвращает число в заданном диапазоне с заданным шагом

### <a name="generateFormData"></a>generateFormData

Универсальная функция формирования formData

*arguments*

- `data` данные для обогащения
- `parentData` хрень, никому не надо (не точно)
- `model` непосредственно модель данных
- `assign` обогащаем ли formData данными из `data`
- `toService` в этом режиме следим за полем exclude модели для удаления нежелательных к отправке полей

```javascript
import { generateFormData } from 'common-components/src/helpers/common'

const generateFormData = ({data, parentData, model, assign = true, toService = false}) => {
  let generate = (data, localModel) => {
    let result = {};
    localModel = R.clone(localModel);
    data = R.clone(data);

    Object.keys(localModel).forEach((key) => {
      let modelValue = localModel[key].value && typeof localModel[key].value === 'function' ? localModel[key].value(data, parentData) : localModel[key].value;
      let isExclude = localModel[key].exclude && typeof localModel[key].exclude === 'function' ? localModel[key].exclude(data, parentData) : localModel[key].exclude;

      if (!(isExclude && toService)) {
        if (localModel[key].nested_fields) {
          result[key] = generate(data && data[key], localModel[key].nested_fields)
        } else if (localModel[key].$each) {
          result[key] = assign && data && data[key] && data[key].length
            ? data[key].map(item => generate(item, localModel[key].$each))
            : [];
        } else {
          result[key] = assign ? data && data[key] || modelValue : modelValue
        }
      }
    })
    return result
  }

  if (Array.isArray(data)) {
    return data.map(generate)
  } else {

    return generate(data, model)
  }
}
```

